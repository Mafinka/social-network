package com.getjavajob.training.socialnetwork.zvyagu.dao;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.constant.AccountRole;
import com.getjavajob.training.socialnetwork.zvyagu.dao.exeptions.RuntimeDaoException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Calendar;
import java.util.Date;
import java.util.Set;

public class AccountProxy extends Account {
    private DaoService service;
    private Account entity;

    public AccountProxy(long id, DaoService service) {
        super.setID(id);
        this.service = service;
    }

    private void loadFromDao() {
        if (!isLoaded()) {
            try {
                service.execute(factory -> entity = factory.getAccountDao().get(getID()));
            } catch (Exception e) {
                throw new RuntimeDaoException(e);
            }
        }
    }

    public boolean isLoaded() {
        return entity != null;
    }

    @Override
    public void setID(long ID) {
        loadFromDao();
        entity.setID(ID);
        super.setID(ID);
    }

    @Override
    public String getFirstName() {
        loadFromDao();
        return entity.getFirstName();
    }

    @Override
    public void setFirstName(String firstName) {
        loadFromDao();
        entity.setFirstName(firstName);
    }

    @Override
    public String getLastName() {
        loadFromDao();
        return entity.getLastName();
    }

    @Override
    public void setLastName(String lastName) {
        loadFromDao();
        entity.setLastName(lastName);
    }

    @Override
    public String getMiddleName() {
        loadFromDao();
        return entity.getMiddleName();
    }

    @Override
    public void setMiddleName(String middleName) {
        loadFromDao();
        entity.setMiddleName(middleName);
    }

    @Override
    public Calendar getBirthday() {
        loadFromDao();
        return entity.getBirthday();
    }

    @Override
    public void setBirthday(Calendar birthday) {
        loadFromDao();
        entity.setBirthday(birthday);
    }

    @Override
    public Date getBirthdayDate() {
        loadFromDao();
        return entity.getBirthdayDate();
    }

    @Override
    public void setBirthday(int year, int month, int day) {
        loadFromDao();
        entity.setBirthday(year, month, day);
    }

    @Override
    public String getEmail() {
        loadFromDao();
        return entity.getEmail();
    }

    @Override
    public void setEmail(String email) {
        loadFromDao();
        entity.setEmail(email);
    }

    @Override
    public String getAddress() {
        loadFromDao();
        return entity.getAddress();
    }

    @Override
    public void setAddress(String address) {
        loadFromDao();
        entity.setAddress(address);
    }

    @Override
    public String getWorkAddress() {
        loadFromDao();
        return entity.getWorkAddress();
    }

    @Override
    public void setWorkAddress(String workAddress) {
        loadFromDao();
        entity.setWorkAddress(workAddress);
    }

    @Override
    public String getIcq() {
        loadFromDao();
        return entity.getIcq();
    }

    @Override
    public void setIcq(String icq) {
        loadFromDao();
        entity.setIcq(icq);
    }

    @Override
    public String getSkype() {
        loadFromDao();
        return entity.getSkype();
    }

    @Override
    public void setSkype(String skype) {
        loadFromDao();
        entity.setSkype(skype);
    }

    @Override
    public String getExtraInfo() {
        loadFromDao();
        return entity.getExtraInfo();
    }

    @Override
    public void setExtraInfo(String extraInfo) {
        loadFromDao();
        entity.setExtraInfo(extraInfo);
    }

    @Override
    public Set<String> getPhones() {
        loadFromDao();
        return entity.getPhones();
    }

    @Override
    public boolean addPhone(String phone) {
        loadFromDao();
        return entity.addPhone(phone);
    }

    @Override
    public boolean removePhone(String phone) {
        loadFromDao();
        return entity.removePhone(phone);
    }

    @Override
    public Set<String> getWorkPhones() {
        loadFromDao();
        return entity.getWorkPhones();
    }

    @Override
    public boolean addWorkPhone(String phone) {
        loadFromDao();
        return entity.addWorkPhone(phone);
    }

    @Override
    public boolean removeWorkPhone(String phone) {
        loadFromDao();
        return entity.removeWorkPhone(phone);
    }

    @Override
    public String getPassword() {
        loadFromDao();
        return entity.getPassword();
    }

    @Override
    public void setPassword(String password) {
        loadFromDao();
        entity.setPassword(password);
    }

    @Override
    public AccountRole getRole() {
        loadFromDao();
        return entity.getRole();
    }

    @Override
    public void setRole(AccountRole role) {
        loadFromDao();
        entity.setRole(role);
    }

    @Override
    public Calendar getRegistrationDate() {
        loadFromDao();
        return entity.getRegistrationDate();
    }

    @Override
    public void setRegistrationDate(Calendar birthday) {
        loadFromDao();
        entity.setRegistrationDate(birthday);
    }

    @Override
    public Date getRegistrationExactlyDate() {
        loadFromDao();
        return super.getRegistrationExactlyDate();
    }

    @Override
    public void setRegistrationDate(int year, int month, int day) {
        loadFromDao();
        entity.setRegistrationDate(year, month, day);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (o instanceof AccountProxy) {
            return super.equals(o);
        } else if (o instanceof Account) {
            loadFromDao();
            return entity.equals(o);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "AccountProxy{" +
                "ID=" + getID() +
                ", entity=" + entity +
                '}';
    }
}
