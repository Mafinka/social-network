package com.getjavajob.training.socialnetwork.zvyagu.common;

import com.getjavajob.training.socialnetwork.zvyagu.common.constant.AccountRole;
import com.getjavajob.training.socialnetwork.zvyagu.common.constant.FriendCategory;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import org.hibernate.LazyInitializationException;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Table(name = "ACCOUNT")
@XStreamAlias("account")
public class Account extends BaseEntity {
    @XStreamOmitField
    @Column(name = "PASSWORD")
    private String password;
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Column(name = "LAST_NAME")
    private String lastName;
    @Column(name = "MIDDLE_NAME")
    private String middleName;
    @Temporal(TemporalType.DATE)
    @Column(name = "BIRTHDAY")
    private Calendar birthday;
    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "PHONES", foreignKey = @ForeignKey(name = "ACCOUNT_ID"))
    @Column(name = "PHONE")
    private Set<String> phones = new HashSet<>();
    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "WORK_PHONES", foreignKey = @ForeignKey(name = "ACCOUNT_ID"))
    @Column(name = "PHONE")
    private Set<String> workPhones = new HashSet<>();
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "ADDRESS")
    private String address;
    @Column(name = "WORK_ADDRESS")
    private String workAddress;
    @Column(name = "ICQ")
    private String icq;
    @Column(name = "SKYPE")
    private String skype;
    @Column(name = "INFO")
    private String extraInfo;
    @XStreamOmitField
    @Enumerated(EnumType.STRING)
    @Column(name = "ROLE")
    private AccountRole role = AccountRole.USER;
    @XStreamOmitField
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "REGISTRATION_DATE")
    private Calendar registrationDate;
    @XStreamOmitField
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "ACCOUNT_IMAGES",
            joinColumns = @JoinColumn(name = "ACCOUNT_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "IMAGE_ID", referencedColumnName = "ID")
    )
    private Image image;
    @XStreamOmitField
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACCOUNT_ID", referencedColumnName = "ID")
    private Set<Friend> friends;

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Set<Friend> getFriends() {
        return friends;
    }

    public void setFriends(Set<Friend> friends) {
        this.friends = friends;
    }

    public void addFriend(Account account, FriendCategory category) {
        if (account.getID() != getID()) {
            friends.add(new Friend(this, account, category));
        } else {
            throw new IllegalArgumentException("account could not be friend to himself");
        }
    }

    public void removeFriend(Account account) {
        friends.remove(new Friend(this, account));
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Calendar getBirthday() {
        Calendar date = Calendar.getInstance();
        date.setTime(birthday.getTime());
        return date;
    }

    public void setBirthday(Calendar birthday) {
        this.birthday = birthday;
    }

    public Date getBirthdayDate() {
        return birthday == null ? null : birthday.getTime();
    }

    public void setBirthday(int year, int month, int day) {
        setBirthday(createDate(year, month, day));
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }

    public Set<String> getPhones() {
        return Collections.unmodifiableSet(phones);
    }

    public void setPhones(Collection<String> phones) {
        this.phones.clear();
        this.phones.addAll(phones.stream().filter(Objects::nonNull).collect(Collectors.toSet()));
    }

    public boolean addPhone(String phone) {
        return phones.add(phone);
    }

    public boolean removePhone(String phone) {
        return phones.remove(phone);
    }

    public Set<String> getWorkPhones() {
        return Collections.unmodifiableSet(workPhones);
    }

    public void setWorkPhones(Collection<String> workPhones) {
        this.workPhones.clear();
        this.workPhones.addAll(workPhones.stream().filter(Objects::nonNull).collect(Collectors.toSet()));
    }

    public boolean addWorkPhone(String phone) {
        return workPhones.add(phone);
    }

    public boolean removeWorkPhone(String phone) {
        return workPhones.remove(phone);
    }

    public AccountRole getRole() {
        return role;
    }

    public void setRole(AccountRole role) {
        if (role == null) {
            this.role = AccountRole.USER;
        } else {
            this.role = role;
        }
    }

    public Calendar getRegistrationDate() {
        Calendar date = Calendar.getInstance();
        date.setTime(registrationDate.getTime());
        return date;
    }

    public void setRegistrationDate(Calendar birthday) {
        this.registrationDate = birthday;
    }

    public Date getRegistrationExactlyDate() {
        return registrationDate == null ? null : registrationDate.getTime();
    }

    public void setRegistrationDate(int year, int month, int day) {
        setRegistrationDate(createDate(year, month, day));
    }

    private Calendar createDate(int year, int month, int day) {
        Calendar date = Calendar.getInstance();
        date.set(Calendar.MILLISECOND, 0);
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.YEAR, year);
        date.set(Calendar.MONTH, month - 1);
        date.set(Calendar.DAY_OF_MONTH, day);
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        // allow comparison with the inheritors
        if (o == null || !(o instanceof Account)) return false;

        Account account = (Account) o;
        if (!super.equals(o)) return false;
        if (getFirstName() != null ? !getFirstName().equals(account.getFirstName()) : account.getFirstName() != null)
            return false;
        if (getLastName() != null ? !getLastName().equals(account.getLastName()) : account.getLastName() != null)
            return false;
        if (getMiddleName() != null ? !getMiddleName().equals(account.getMiddleName()) : account.getMiddleName() != null)
            return false;
        if (getBirthday() == null && account.getBirthday() != null
                || getBirthday() != null && account.getBirthday() == null
                || getBirthday() != null &&
                (getBirthday().get(Calendar.YEAR) != account.getBirthday().get(Calendar.YEAR) ||
                        getBirthday().get(Calendar.DAY_OF_YEAR) != account.getBirthday().get(Calendar.DAY_OF_YEAR))) {
            return false;
        }
//        if (getPhones() != null ? !getPhones().equals(account.getPhones()) : account.getPhones() != null) return false;
//        if (getWorkPhones() != null ? !getWorkPhones().equals(account.getWorkPhones()) : account.getWorkPhones() != null)
//            return false;
        if (getEmail() != null ? !getEmail().equals(account.getEmail()) : account.getEmail() != null) return false;
        if (getAddress() != null ? !getAddress().equals(account.getAddress()) : account.getAddress() != null)
            return false;
        if (getWorkAddress() != null ? !getWorkAddress().equals(account.getWorkAddress()) : account.getWorkAddress() != null)
            return false;
        if (getIcq() != null ? !getIcq().equals(account.getIcq()) : account.getIcq() != null) return false;
        if (getSkype() != null ? !getSkype().equals(account.getSkype()) : account.getSkype() != null) return false;
        if (getPassword() != null ? !getPassword().equals(account.getPassword()) : account.getPassword() != null)
            return false;
        if (getRole() != null ? !getRole().equals(account.getRole()) : account.getRole() != null) return false;
        return getExtraInfo() != null ? getExtraInfo().equals(account.getExtraInfo()) : account.getExtraInfo() == null;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Account{");
        sb.append("ID=").append(getID()).append(", firstName='").append(firstName).append('\'')
                .append(", lastName='").append(lastName).append('\'').append(", middleName='").append(middleName)
                .append('\'').append(", birthday=").append(birthday == null ? null : birthday.getTime());
        sb.append(", phones=");
        try {
            sb.append(phones);
        } catch (LazyInitializationException ignore) {
        }
        sb.append(", workPhones=");
        try {
            sb.append(workPhones);
        } catch (LazyInitializationException ignore) {
        }
        sb.append(", email='").append(email).append('\'').append(", address='").append(address).append('\'')
                .append(", workAddress='").append(workAddress).append('\'').append(", icq=").append(icq)
                .append(", skype='").append(skype).append('\'').append(", extraInfo='").append(extraInfo).append('\'')
                .append(", password='").append(password).append('\'').append(", role='").append(role).append('\'')
                .append(", registrationDate='").append(registrationDate == null ? null : registrationDate.getTime()).append('}');
        return sb.toString();
    }
}
