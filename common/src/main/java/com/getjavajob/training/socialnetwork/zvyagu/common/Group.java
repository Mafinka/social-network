package com.getjavajob.training.socialnetwork.zvyagu.common;

import javax.persistence.*;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "GROUPS")
@NamedQueries({
        @NamedQuery(
                name = "Group.getAccountGroups",
                query = "SELECT g FROM Group g JOIN FETCH g.members m WHERE m=:acc"
        )
})
public class Group extends BaseEntity {
    @Column(name = "GROUP_NAME")
    private String name;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OWNER", referencedColumnName = "ID")
    private Account owner;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "GROUP_MEMBERS",
            joinColumns = @JoinColumn(name = "GROUP_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "ACCOUNT_ID", referencedColumnName = "ID")
    )
    private Set<Account> members = new LinkedHashSet<>();
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "GROUP_IMAGES",
            joinColumns = @JoinColumn(name = "GROUP_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "IMAGE_ID", referencedColumnName = "ID")
    )
    private Image image;
    @Column(name = "DESCRIPTION")
    private String description;

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Account getOwner() {
        return owner;
    }

    public void setOwner(Account owner) {
        this.owner = owner;
    }

    public Set<Account> getMembers() {
        return Collections.unmodifiableSet(members);
    }

    public boolean addMember(Account member) {
        return members.add(member);
    }

    public boolean removeMember(Account member) {
        return members.remove(member);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Group group = (Group) o;

        if (name != null ? !name.equals(group.name) : group.name != null) return false;
        if (owner != null ? !owner.equals(group.owner) : group.owner != null) return false;
        return members != null ? members.equals(group.members) : group.members == null;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + getID() +
                ", name='" + name + '\'' +
                ", owner=" + owner +
//                ", members=" + members +
//                ", image=" + image +
                ", description='" + description + '\'' +
                '}';
    }
}
