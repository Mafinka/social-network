package com.getjavajob.training.socialnetwork.zvyagu.ui.web;

public abstract class AccountPageContent {
    public static final String ATTR = "page";
    public static final int INFO = 0;
    public static final int EDIT = 1;
}
