package com.getjavajob.training.socialnetwork.zvyagu.dao.sql;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.Friend;
import com.getjavajob.training.socialnetwork.zvyagu.common.constant.FriendCategory;
import com.getjavajob.training.socialnetwork.zvyagu.dao.AccountDao;
import com.getjavajob.training.socialnetwork.zvyagu.dao.exeptions.DaoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.*;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

@Repository("accountDao")
public class SQLAccountDao extends SQLAbstractDao<Account> implements AccountDao {
    private static final Logger logger = LoggerFactory.getLogger(SQLAccountDao.class);

    @Autowired
    public SQLAccountDao() {
        super();
    }

    @Override
    public Set<Account> search(String[] words, int limit, int offset) throws DaoException {
        logger.debug("searching accounts by words {} with limit {} and offset {}", Arrays.toString(words), limit, offset);
        EntityManager manager = getEntityManager();
        CriteriaBuilder builder = manager.getCriteriaBuilder();
        CriteriaQuery<Account> query = builder.createQuery(Account.class);
        Root<Account> root = query.from(Account.class);
        query.select(root);
        searchPredicate(words, builder, query, root);
        return manager.createQuery(query).setMaxResults(limit).setFirstResult(offset).getResultList().stream().collect(Collectors.toSet());
    }

    @Override
    public long searchCount(String[] words) throws DaoException {
        logger.debug("counting accounts from search by words {}", Arrays.toString(words));
        EntityManager manager = getEntityManager();
        CriteriaBuilder builder = manager.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<Account> root = query.from(Account.class);
        query.select(builder.count(root));
        searchPredicate(words, builder, query, root);
        return manager.createQuery(query).getSingleResult();
    }

    private void searchPredicate(String[] words, CriteriaBuilder builder, CriteriaQuery<?> query, Root<Account> root) {
        if (words != null && words.length > 0) {
            Predicate wherePredicate = builder.conjunction();
            for (int i = 0; i < words.length; i++) {
                wherePredicate = builder.and(wherePredicate, builder.or(
                        builder.like(root.get("firstName"), '%' + words[i] + '%'),
                        builder.like(root.get("lastName"), '%' + words[i] + '%'),
                        builder.like(root.get("middleName"), '%' + words[i] + '%')
                ));
            }
            query.where(wherePredicate);
        }
    }

    @Override
    public Account getByEmail(String email) throws DaoException {
        logger.debug("getting by email '{}'", email);
        EntityManager manager = getEntityManager();
        try {
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<Account> query = builder.createQuery(Account.class);
            Root<Account> root = query.from(Account.class);
            query.select(root).where(builder.equal(root.get("email"), email));
            return manager.createQuery(query).getSingleResult();
        } catch (NoResultException e) {
            return null;
        } catch (Exception e) {
            logger.error("error getting by email '{}'", email, e);
            throw new DaoException(e);
        }
    }

    @Override
    public Set<Friend> getFriends(Account account) throws DaoException {
        logger.debug("getting friends for account {}", account);
        try {
            return validateEntity(account).getFriends();
        } catch (Exception e) {
            logger.error("error getting friends for account {}", account);
            throw new DaoException(e);
        }
    }

    @Override
    public Set<Friend> getFriends(Account account, int limit, int offset) throws DaoException {
        try {
            EntityManager manager = getEntityManager();
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<Friend> query = builder.createQuery(Friend.class);
            Root<Account> accountRoot = query.from(Account.class);
            query.where(builder.equal(accountRoot.get("id"), account.getID()));
            SetJoin<Account, Friend> join = accountRoot.joinSet("friends");
            CriteriaQuery<Friend> q = query.select(join);
            return manager.createQuery(q).setMaxResults(limit).setFirstResult(offset).getResultList().stream().collect(Collectors.toSet());
        } catch (Exception e) {
            logger.error("error getting friends for account {} with limit {} and offset {}", account.getID(), limit, offset, e);
            throw new DaoException(e);
        }
    }

    @Override
    public void addFriend(Account account, Account friend, FriendCategory category) throws DaoException {
        logger.debug("adding friend {} with category {} to account {}", friend.getID(), category, account.getID());
        validateEntity(account).addFriend(friend, category);
    }

    @Override
    public void removeFriend(Account account, Account friend) throws DaoException {
        logger.debug("removing friend {} from account {}", friend, account);
        validateEntity(account).removeFriend(friend);
    }

    @Override
    protected Class<Account> getEntityClass() {
        return Account.class;
    }
}
