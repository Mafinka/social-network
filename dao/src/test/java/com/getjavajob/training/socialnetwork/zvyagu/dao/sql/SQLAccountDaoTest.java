package com.getjavajob.training.socialnetwork.zvyagu.dao.sql;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.Friend;
import com.getjavajob.training.socialnetwork.zvyagu.common.constant.AccountRole;
import com.getjavajob.training.socialnetwork.zvyagu.common.constant.FriendCategory;
import com.getjavajob.training.socialnetwork.zvyagu.dao.AccountDao;
import com.getjavajob.training.socialnetwork.zvyagu.dao.exeptions.DaoException;
import org.h2.tools.RunScript;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import static com.getjavajob.training.socialnetwork.zvyagu.dao.sql.SQLDaoTestUtil.DATABASE_INIT;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:dao-context-override.xml"})
public class SQLAccountDaoTest {
    @Autowired
    private AccountDao accountDao;
    @Autowired
    private DataSource dataSource;
    @Autowired
    private SQLDaoFactoryImpl factory;

    private static Account getCreateTestAccount() {
        Account account = new Account();
//        account.setUsername("semen92");
        account.setFirstName("Semen");
        account.setLastName("Semenov");
        account.setMiddleName("Semenovich");
        account.setBirthday(1992, 1, 10);
        account.setEmail("semen92@ya.ru");
        account.setAddress("Privolnaya, 23");
        account.setWorkAddress("Privolnaya, 39");
        account.setIcq("87654321");
        account.setSkype("semen92");
        account.addPhone("+79059998877");
        account.addPhone("+79059998876");
        account.addWorkPhone("+79059998875");
        account.addWorkPhone("+79059998874");
        account.setPassword("@semen@");
        account.setRole(AccountRole.USER);
        return account;
    }

    @SuppressWarnings("Duplicates")
    public static Set<Account> getTestAccounts() {
        Set<Account> result = new LinkedHashSet<>();
        result.add(getFirstAccount());
        result.add(getSecondAccount());
        result.add(getThirdAccount());
        result.add(getSuperUser());
        return result;
    }

    public static Account getSuperUser() {
        Account account = new Account();
        account.setID(4);
//        account.setUsername("su");
        account.setFirstName("Master");
        account.setLastName("Overlord");
        account.setMiddleName("Admin");
        account.setBirthday(2200, 1, 1);
        account.setEmail("su@mail.com");
        account.setAddress("Galaxy");
        account.setWorkAddress("social-network");
        account.setIcq("99999999");
        account.setSkype("su");
        account.setExtraInfo("super user");
        account.addPhone("+79059998841");
        account.addPhone("+79059998842");
        account.addWorkPhone("+79059998843");
        account.addWorkPhone("+79059998844");
        account.setPassword("superuser");
        account.setRole(AccountRole.ADMIN);
        return account;
    }

    @SuppressWarnings("Duplicates")
    public static Account getFirstAccount() {
        Account account = new Account();
        account.setID(1);
//        account.setUsername("ivan91");
        account.setFirstName("Ivan");
        account.setLastName("Ivanov");
        account.setMiddleName("Ivanovich");
        account.setBirthday(1991, 2, 8);
        account.setEmail("ivan91@ya.ru");
        account.setAddress("Privolnaya, 57");
        account.setWorkAddress("Privolnaya, 58");
        account.setIcq("12345678");
        account.setSkype("ivan91");
        account.setExtraInfo("extra");
        account.addPhone("+79059998811");
        account.addPhone("+79059998812");
        account.addWorkPhone("+79059998813");
        account.addWorkPhone("+79059998814");
        account.setPassword("ivan1234");
        account.setRole(AccountRole.USER);
        return account;
    }

    @SuppressWarnings("Duplicates")
    public static Account getSecondAccount() {
        Account account = new Account();
        account.setID(2);
//        account.setUsername("petr83");
        account.setFirstName("Petr");
        account.setLastName("Petrov");
        account.setMiddleName("Petrovich");
        account.setBirthday(1983, 12, 31);
        account.setEmail("-petr83-@ya.ru");
        account.setAddress("Moscow");
        account.setWorkAddress("Moscow");
        account.setIcq("12344321");
        account.setSkype("petr83");
        account.setExtraInfo("");
        account.addPhone("+79059998821");
        account.addPhone("+79059998822");
        account.addWorkPhone("+79059998823");
        account.addWorkPhone("+79059998824");
        account.setPassword("-petrov-");
        account.setRole(AccountRole.USER);
        return account;
    }

    @SuppressWarnings("Duplicates")
    public static Account getThirdAccount() {
        Account account = new Account();
        account.setID(3);
//        account.setUsername("sharikov77");
        account.setFirstName("Poligraph");
        account.setLastName("Sharikov");
        account.setMiddleName("Poligraphich");
        account.setBirthday(1977, 6, 12);
        account.setEmail("sharikov77@ya.ru");
        account.setAddress("Lenina, 32");
        account.setWorkAddress("Lenina, 44");
        account.setIcq("12346543");
        account.setSkype("sharikov77");
        account.setExtraInfo(null);
        account.addPhone("+79059998831");
        account.addPhone("+79059998832");
        account.addWorkPhone("+79059998833");
        account.addWorkPhone("+79059998834");
        account.setPassword("poligraf");
        account.setRole(AccountRole.USER);
        return account;
    }

    @Before
    public void init() throws IOException, SQLException, InterruptedException {
        try (Connection connection = dataSource.getConnection()) {
            RunScript.execute(connection, new InputStreamReader(
                    new BufferedInputStream(ClassLoader.getSystemResourceAsStream(DATABASE_INIT))));
        }
        accountDao = factory.getAccountDao();
    }

    @After
    public void free() throws Exception {
        factory.close();
    }

    @Test
    @Transactional
    public void getAll() throws Exception {
        Set<Account> accounts = accountDao.getAll();
        assertEquals(getTestAccounts(), accounts);
    }

    @Test
    @Transactional
    public void createQuery() throws Exception {
        Account account = getCreateTestAccount();
        assertEquals(0, account.getID());
        accountDao.create(account);
        long id = account.getID();
        assertNotEquals(0, id);

        Account checkAccount = getCreateTestAccount();
        checkAccount.setID(id);
        assertEquals(checkAccount, accountDao.get(id));
    }

    @Test
    public void getQuery() throws DaoException {
        Account account = accountDao.get(1);
        Account checkAccount = getFirstAccount();
        assertEquals(checkAccount, account);
        assertNull(accountDao.get(100));
        assertNull(accountDao.get(0));
        assertNull(accountDao.get(-1));
    }

    @Test
    @Transactional
    public void getByEmail() throws Exception {
        Account account = accountDao.getByEmail("-petr83-@ya.ru");
        Account checkAccount = getSecondAccount();
        assertEquals(checkAccount, account);
        assertNull(accountDao.getByEmail("abc"));
        assertNull(accountDao.getByEmail(null));
    }

    @Test
    @Transactional
    public void getFriends() throws Exception {
        Account account = accountDao.get(2);
        Set<Friend> result = new HashSet<>();
        Set<Friend> checkSet = new HashSet<>();
        checkSet.add(new Friend(account, getFirstAccount(), FriendCategory.FRIEND));
        checkSet.add(new Friend(account, getThirdAccount(), FriendCategory.UNIVERSITY));
        result.addAll(accountDao.getFriends(account));
        assertEquals(checkSet, result);
    }

    @Test
    @Transactional
    public void addFriend() throws Exception {
        Account friendAccount = accountDao.get(3);
        Set<Friend> friends = new HashSet<>();
        Account account = accountDao.get(1);
        Friend friend = new Friend(account, friendAccount);
        accountDao.addFriend(account, friendAccount);
        friends.addAll(accountDao.getFriends(account));
        assertEquals(2, friends.size());
        assertTrue(friends.contains(friend));
    }

    @Test
    @Transactional
    public void reAddFriend() throws Exception {
        Account account = accountDao.get(2);
        accountDao.addFriend(account, accountDao.get(3));
    }

    @Test
    @Transactional
    public void removeFriend() throws Exception {
        Account friendAccount = accountDao.get(3);
        Account account = accountDao.get(2);
        Friend friend = new Friend(account, friendAccount);
        // double remove
        accountDao.removeFriend(account, friendAccount);
        accountDao.removeFriend(account, friendAccount);
        Set<Friend> friends = accountDao.getFriends(accountDao.get(2));
        assertEquals(1, friends.size());
        assertFalse(friends.contains(friend));
    }

    @Test
    @Transactional
    public void updateQuery() throws Exception {
        Account account = getSecondAccount();
        account.setExtraInfo("test101");
        account.setBirthday(1982, 11, 28);
        account.addPhone("+79059998825");
        account.addPhone("+79059998826");
        account.removePhone("+79059998822");
        account.addWorkPhone("+79059998827");
        account.removeWorkPhone("+79059998823");
        accountDao.update(account);
        assertEquals(account, accountDao.get(account.getID()));
    }

    @Test
    @Transactional
    public void deleteQuery() throws Exception {
        deleteAccount(3);
        assertEquals(null, accountDao.get(3));
    }

    @Transactional
    private void deleteAccount(long id) throws DaoException {
        accountDao.delete(id);
    }

    @Test
    @Transactional
    public void searchEmpty() throws Exception {
        Set<Account> result = accountDao.search(new String[0]);
        assertEquals(4, result.size());
    }

    @Test
    @Transactional
    public void searchNull() throws Exception {
        Set<Account> result = accountDao.search(null);
        assertEquals(4, result.size());
    }

    @Test
    @Transactional
    public void searchOneWord() throws Exception {
        Set<Account> result = accountDao.search(new String[]{"Admin"});
        assertEquals(1, result.size());
        assertTrue(result.contains(getSuperUser()));

        result = accountDao.search(new String[]{"Over"});
        assertEquals(1, result.size());
        assertTrue(result.contains(getSuperUser()));

        result = accountDao.search(new String[]{"ter"});
        assertEquals(1, result.size());
        assertTrue(result.contains(getSuperUser()));
    }

    @Test
    @Transactional
    public void searchManyWords() throws Exception {
        Set<Account> result = accountDao.search(new String[]{"Admin", "Over", "ter"});
        assertEquals(1, result.size());
        assertTrue(result.contains(getSuperUser()));
    }

    @Test
    @Transactional
    public void searchCountOneWord() throws Exception {
        long result = accountDao.searchCount(new String[]{"Admin"});
        assertEquals(1, result);

        result = accountDao.searchCount(new String[]{"Over"});
        assertEquals(1, result);

        result = accountDao.searchCount(new String[]{"ter"});
        assertEquals(1, result);
    }

    @Test
    @Transactional
    public void searchCountManyWords() throws Exception {
        long result = accountDao.searchCount(new String[]{"Admin", "Over", "ter"});
        assertEquals(1, result);
    }

    @Test
    @Transactional
    public void searchCountNull() throws Exception {
        long result = accountDao.searchCount(null);
        assertEquals(4, result);
    }

    @Test
    @Transactional
    public void searchCountEmpty() throws Exception {
        long result = accountDao.searchCount(new String[0]);
        assertEquals(4, result);
    }

    @Test
    @Transactional
    public void getFriendsWithLimit() throws Exception {
        assertEquals(2, accountDao.getFriends(getSecondAccount(), 10, 0).size());
        assertEquals(1, accountDao.getFriends(getSecondAccount(), 1, 0).size());
    }
}
