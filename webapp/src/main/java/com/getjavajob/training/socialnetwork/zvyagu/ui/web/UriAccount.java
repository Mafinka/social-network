package com.getjavajob.training.socialnetwork.zvyagu.ui.web;

public abstract class UriAccount {
    public static final String HOME = "/home";
    public static final String REGISTRATION = "/registration";
    public static final String UPDATE = "/update";
    public static final String CREATE = "/create";
    public static final String EDIT = "/edit";
    public static final String XML_UPLOAD = "/xml";
    public static final String XML_LOAD = "/to_xml";
}
