$(document).ready(function () {
    // add/remove phone

    function attachDeletePhoneButtonListeners() {
        $(".del-phone-btn").on("click", function (event) {
            var $target = $(event.target);
            $target.parent().parent().remove();
        });
    }

    function attachPhoneMask() {
        $(".phone-input").inputmask({"mask": "+7 (999) 999-99-99"}); //specifying options
    }

    function attachAddPhoneButtonListeners() {
        $(".add-phone-btn").on("click", function (event) {
            var $target = $(event.target);
            var $phonesBlock = $target.parent().find(".phonesBlock");
            // var $phonesBlock = $target.parent();
            var name = $target.attr('name');
            var phoneTemplate = $("#phoneTemplate").html();
            $phonesBlock.append(phoneTemplate.replace('%NAME%', name).replace('%CLAZZ%', name.substr(0, name.length - 1)));
            attachDeletePhoneButtonListeners();

            attachPhoneMask();
        });
    }

    function attachPhoneEvents() {
        attachDeletePhoneButtonListeners();
        attachPhoneMask();
        attachAddPhoneButtonListeners();
    }


    // Confirmation Edit Account Form
    function initConfirmEditAccountDialog() {
        $("#dialog-confirm").dialog({
            autoOpen: false,
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                Confirm: function () {
                    $(this).dialog("close");
                    var $form = $(".input-form");
                    if ($form.parsley().validate({
                            group: "account"
                        })) {
                        console.log(true);
                        var counter = 0;
                        $(".phone").each(function () {
                            var $cur = $(this);
                            $cur.attr("name", $cur.attr("name") + "[" + counter++ + "]");
                        });
                        counter = 0;
                        $(".workPhone").each(function () {
                            var $cur = $(this);
                            $cur.attr("name", $cur.attr("name") + "[" + counter++ + "]");
                        });
                        $form.submit();
                    }
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
    }

    function attachConfirmEditAccountButtonListener() {
        $(".form-submit-button").on("click", function () {
            $( "#dialog-confirm" ).dialog( "open" );
        });
    }

    function initEditAccountForm() {
        attachPhoneEvents();
        initConfirmEditAccountDialog();
        attachConfirmEditAccountButtonListener();
    }



    initEditAccountForm();



    // add photo customize
    $(function () {
        var wrapper = $(".photo_upload"),
            inp = wrapper.find("input"),
            btn = wrapper.find("button"),
            lbl = wrapper.find("div");

        btn.focus(function () {
            inp.focus()
        });
        // Crutches for the :focus style:
        inp.focus(function () {
            wrapper.addClass("focus");
        }).blur(function () {
            wrapper.removeClass("focus");
        });

        var file_api = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;

        inp.change(function () {
            var file_name;
            if (file_api && inp[0].files[0])
                file_name = inp[0].files[0].name;
            else
                file_name = inp.val().replace("C:\\fakepath\\", '');

            if (!file_name.length)
                return;

            if (lbl.is(":visible")) {
                lbl.text(file_name);
                btn.text("Choose photo");
            } else
                btn.text(file_name);
        }).change();

    });

    $(window).resize(function () {
        $(".file_upload input").triggerHandler("change");
    });




    // datepicker
    var year = new Date().getFullYear();
    var $field = $(".datePicker");

    var strDate = $field.val();

    if (!(strDate === undefined)) {
        var dateParts = strDate.split("-");
        var date = new Date(dateParts[0], (dateParts[1] - 1), dateParts[2]);

        $.datepicker.setDefaults( $.datepicker.regional[ "ru" ] );
        $field.attr('readonly', '');
        $field.datepicker({
            dateFormat: 'yy.mm.dd',
            defaultDate: date,
            changeYear: true,
            changeMonth: true,
            yearRange: "1900:" + year
        });
    }



    // search tabbed
    $( "#tabs" ).tabs();


    // search autocomplete
    $(function () {
        $.widget( "custom.catcomplete", $.ui.autocomplete, {
            _create: function() {
                this._super();
                this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
            },
            _renderMenu: function( ul, items ) {
                var that = this,
                    currentCategory = "";
                $.each( items, function( index, item ) {
                    var li;
                    if ( item.category != currentCategory ) {
                        ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
                        currentCategory = item.category;
                    }
                    li = that._renderItemData( ul, item );
                    if ( item.category ) {
                        li.attr( "aria-label", item.category + " : " + item.label );
                    }
                });
            }
        });

        $('.top-menu__search-input').catcomplete(
            {
                delay: 0,
                source: function(request, response) {
                    $.ajax({
                        url: '/search/auto',
                        data: {
                            words: request.term
                        },
                        success: function(data) {
                            response($.map(data, function(searchContainer, i) {
                                console.log(searchContainer);
                                return {
                                    id: searchContainer.id,
                                    url: searchContainer.url,
                                    value: searchContainer.label,
                                    label: searchContainer.label,
                                    category: searchContainer.category
                                }
                            }));
                        }
                    });
                },
                minLength: 1,
                select: function (event, ui) {
                    window.location.href = ui.item.url;
                    // window.open(ui.item.url);
                }
            }
        );
    });




    // search page
    var accountsMaxPages = $(".accounts-buttons").attr("data-page-count");
    var groupsMaxPages = $(".groups-buttons").attr("data-page-count");
    var accountsPage = 1;
    var groupsPage = 1;
    var words = $('.top-menu__search-input').attr('value');
    $( document ).ready(function () {
        $(".accounts-buttons").on("click", function (event) {
            switch (event.target.name) {
                case 'begin':
                    accountsPage = 1;
                    break;
                case 'end':
                    accountsPage = accountsMaxPages;
                    break;
                case 'next':
                    accountsPage = Math.min(accountsPage + 1, accountsMaxPages);
                    break;
                case 'prev':
                    accountsPage = Math.max(accountsPage - 1, 1);
                    break;
            }
            $.post({
                url: '/search/accounts/' + accountsPage,
                data: {
                    words: words
                },
                success: function(data) {
                    $(".accounts-content").replaceWith(data);
                    $(".accounts-page-counter").text(accountsPage);
                }
            });
        });
        $(".groups-buttons").on("click", function (event) {
            switch (event.target.name) {
                case 'begin':
                    groupsPage = 1;
                    break;
                case 'end':
                    groupsPage = groupsMaxPages;
                    break;
                case 'next':
                    groupsPage = Math.min(groupsPage + 1, groupsMaxPages);
                    break;
                case 'prev':
                    groupsPage = Math.max(groupsPage - 1, 1);
                    break;
            }
            $.post({
                url: '/search/groups/' + groupsPage,
                data: {
                    words: words
                },
                success: function(data) {
                    $(".groups-content").replaceWith(data);
                    $(".groups-page-counter").text(groupsPage);
                }
            });
        });
    });


    //load xml buttons
    $(function () {
        var wrapper = $(".xml_upload");
        var inp = wrapper.find("input");
        var btn = wrapper.find("button");
        var lbl = wrapper.find("div");

        btn.focus(function () {
            inp.focus()
        });
        // Crutches for the :focus style:
        inp.focus(function () {
            wrapper.addClass("focus");
        }).blur(function () {
            wrapper.removeClass("focus");
        });

        var file_api = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;

        inp.change(function () {
            var file_name;
            if (file_api && inp[0].files[0])
                file_name = inp[0].files[0].name;
            else
                file_name = inp.val().replace("C:\\fakepath\\", '');

            if (!file_name.length)
                return;

            console.log(file_name);
            var file = inp[0].files[0];
            console.log(file);
            var data = new FormData();
            data.append("xml", file);

            $.post({
                url: inp[0].getAttribute('data-page-uri') + '/xml',
                data: data,
                processData: false,
                contentType: false,
                success: function (data) {
                    $(".account_edit_div").replaceWith(data);
                    initEditAccountForm();
                }
            });
        }).change();
    });
});