package com.getjavajob.training.socialnetwork.zvyagu.common.constant;

public enum AccountRole {
    USER,
    ADMIN
}
