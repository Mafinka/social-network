package com.getjavajob.training.socialnetwork.zvyagu.common;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

import javax.persistence.*;

@MappedSuperclass
public abstract class BaseEntity {
    @XStreamOmitField
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    public long getID() {
        return id;
    }

    public void setID(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof BaseEntity)) return false;

        BaseEntity baseEntity = (BaseEntity) o;

        return getID() == baseEntity.getID();
    }

    @Override
    public int hashCode() {
        return (int) (getID() ^ (getID() >>> 32));
    }
}
