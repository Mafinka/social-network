<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-list"></span>
        Group information
    </div>
    <div class="panel-body">
        <div class="row">
            <label class="col-md-4">Group name:</label>
            <div class="col-md-8">${group.name}</div>
        </div>
        <div class="row">
            <label class="col-md-4">Owner:</label>
            <div class="col-md-8">${group.owner.lastName} ${group.owner.firstName}</div>
        </div>
        <div class="row group-description">
            <label class="col-sm-4">Description:</label>
            <div class="col-sm-8">${group.description}</div>
            <%--<div class="col-sm-2">--%>
                <%--<button type="button" class="btn btn-default btn-block">--%>
                    <%--&nbsp;<span class="glyphicon glyphicon-edit"></span>&nbsp;--%>
                <%--</button>--%>
            <%--</div>--%>
        </div>
    </div>
</div>