package com.getjavajob.training.socialnetwork.zvyagu.ui.web.wrap;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.BaseEntity;
import com.getjavajob.training.socialnetwork.zvyagu.common.constant.AccountRole;
import org.springframework.util.StringUtils;

import java.util.*;

public class AccountWrapper extends BaseEntity {
    private long id;
    private String password;
    private String firstName;
    private String lastName;
    private String middleName;
    private Calendar birthday;
    private List<PhoneWrapper> phones = new ArrayList<>();
    private List<PhoneWrapper> workPhones = new ArrayList<>();
    private String email;
    private String address;
    private String workAddress;
    private String icq;
    private String skype;
    private String extraInfo;
    private AccountRole role = AccountRole.ADMIN;
    private Account account;
//    private Calendar registrationDate;

    public void setAccount(Account account) {
        this.account = account;
    }

    public Account getAccount() {
//        account.setID(getID());
        account.setPassword(getPassword());
        account.setEmail(getEmail());
        account.setFirstName(getFirstName());
        account.setLastName(getLastName());
        account.setMiddleName(getMiddleName());
        account.setBirthday(getBirthday());
        for (PhoneWrapper phoneWrapper : getPhones()) {
            if (StringUtils.hasText(phoneWrapper.getPhone())) {
                account.addPhone(phoneWrapper.getPhone());
            }
        }
        for (PhoneWrapper phoneWrapper : getWorkPhones()) {
            if (StringUtils.hasText(phoneWrapper.getPhone())) {
                account.addWorkPhone(phoneWrapper.getPhone());
            }
        }
        account.setAddress(getAddress());
        account.setWorkAddress(getWorkAddress());
        account.setIcq(getIcq());
        account.setSkype(getSkype());
        account.setExtraInfo(getExtraInfo());
        // TODO: 28.02.2017 add role param
//        account.setRole(getRole());
        return account;
    }

    @Override
    public long getID() {
        return id;
    }

    @Override
    public void setID(long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Calendar getBirthday() {
        return birthday;
    }

    public void setBirthday(Calendar birthday) {
        this.birthday = birthday;
    }

    public List<PhoneWrapper> getPhones() {
        return phones;
    }

    public void setPhones(List<PhoneWrapper> phones) {
        this.phones = phones;
    }

    public List<PhoneWrapper> getWorkPhones() {
        return workPhones;
    }

    public void setWorkPhones(List<PhoneWrapper> workPhones) {
        this.workPhones = workPhones;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }

    public AccountRole getRole() {
        return role;
    }

    public void setRole(AccountRole role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "AccountWrapper{" +
                "id=" + getID() +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", birthday=" + birthday +
                ", phones=" + phones +
                ", workPhones=" + workPhones +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", icq='" + icq + '\'' +
                ", skype='" + skype + '\'' +
                ", extraInfo='" + extraInfo + '\'' +
                ", role=" + role +
                '}';
    }
}
