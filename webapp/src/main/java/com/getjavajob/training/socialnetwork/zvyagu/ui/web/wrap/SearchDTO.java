package com.getjavajob.training.socialnetwork.zvyagu.ui.web.wrap;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.Group;

public class SearchDTO {
    private long id;
    private String label;
    private String category;
    private String url;

    public SearchDTO(Account account, String url) {
        id = account.getID();
        label = account.getLastName() + ' ' + account.getFirstName();
        if (account.getMiddleName() != null && !account.getMiddleName().isEmpty()) {
            label += ' ' + account.getMiddleName();
        }
        category = account.getClass().getSimpleName() + 's';
        this.url = url;
    }

    public SearchDTO(Group group, String url) {
        id = group.getID();
        label = group.getName();
        category = group.getClass().getSimpleName() + 's';
        this.url = url;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
