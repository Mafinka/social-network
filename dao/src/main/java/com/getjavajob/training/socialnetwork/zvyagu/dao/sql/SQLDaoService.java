package com.getjavajob.training.socialnetwork.zvyagu.dao.sql;

import com.getjavajob.training.socialnetwork.zvyagu.dao.DaoFactory;
import com.getjavajob.training.socialnetwork.zvyagu.dao.DaoService;
import com.getjavajob.training.socialnetwork.zvyagu.dao.Executor;
import com.getjavajob.training.socialnetwork.zvyagu.dao.QueryExecutor;
import com.getjavajob.training.socialnetwork.zvyagu.dao.exeptions.DaoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("daoService")
public class SQLDaoService implements DaoService {
    private static final Logger logger = LoggerFactory.getLogger(SQLDaoService.class);
    private DaoFactory factory;

    public SQLDaoService() {
        logger.info("Initializing dao service");
    }

    private DaoFactory getFactory() throws DaoException {
        return factory;
    }

    @Autowired
    public void setFactory(DaoFactory factory) {
        this.factory = factory;
    }

    @Override
    public void execute(Executor executor) throws Exception {
        logger.debug("executing, capture session");
        exec(executor);
        logger.debug("finish executing");
    }

    @Override
    @Transactional
    public void executeTransaction(Executor executor) throws Exception {
        logger.debug("executing transaction, capture session");
        exec(executor);
        logger.debug("finish transaction");
    }

    private void exec(Executor executor) throws Exception {
        try (DaoFactory daoFactory = getFactory()) {
            executor.execute(daoFactory);
        }
    }

    @Override
    public <O> O executeQuery(QueryExecutor<O> executor) throws Exception {
        logger.debug("executing query, capture session");
        O result = execQuery(executor);
        logger.debug("finish query");
        return result;
    }

    @Override
    @Transactional
    public <O> O executeTransactionQuery(QueryExecutor<O> executor) throws Exception {
        logger.debug("executing transaction query, capture session");
        O result = execQuery(executor);
        logger.debug("finish transaction query");
        return result;
    }

    private <O> O execQuery(QueryExecutor<O> executor) throws Exception {
        try (DaoFactory daoFactory = getFactory()) {
            return executor.executeQuery(daoFactory);
        }
    }

    @Override
    public void close() { }
}
