package com.getjavajob.training.socialnetwork.zvyagu.dao;

public interface QueryExecutor<O> {
    O executeQuery(DaoFactory factory) throws Exception;
}
