package com.getjavajob.training.socialnetwork.zvyagu.ui.web.propertyeditors;

import org.springframework.util.StringUtils;

import java.beans.PropertyEditorSupport;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;

public class CustomCalendarEditor extends PropertyEditorSupport {
    private DateFormat dateFormat;

    public CustomCalendarEditor(DateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }

    @Override
    public String getAsText() {
        Calendar value = (Calendar) getValue();
        return (value != null ? dateFormat.format(value.getTime()) : "");
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        System.out.println("Calendar editor: " + text);
        if (!StringUtils.hasText(text)) {
            setValue(null);
        } else {
            try {
                Calendar value = Calendar.getInstance();
                value.setTime(dateFormat.parse(text));
                setValue(value);
            } catch (ParseException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }
}
