package com.getjavajob.training.socialnetwork.zvyagu.ui.web.controller;

import com.getjavajob.training.socialnetwork.zvyagu.common.Group;
import com.getjavajob.training.socialnetwork.zvyagu.service.Service;
import com.getjavajob.training.socialnetwork.zvyagu.ui.web.Attribute;
import com.getjavajob.training.socialnetwork.zvyagu.ui.web.Page;
import com.getjavajob.training.socialnetwork.zvyagu.ui.web.Uri;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller("groupController")
@RequestMapping(Uri.GROUP)
public class GroupController {
    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);
    private static final int MEMBERS_LIST_LIMIT = 6;
    @Autowired
    private Service service;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String getGroupPage(@PathVariable("id") Long id, Model model) throws Exception {
        try {
            Group group = service.getGroupService().getGroup(id);
            model.addAttribute(Attribute.GROUP, group);
            model.addAttribute(Attribute.MEMBERS, service.getGroupService().getMembers(group, MEMBERS_LIST_LIMIT, 0));
            return Page.GROUP;
        } catch (Exception e) {
            logger.error("error getting group page for group {}", id, e);
            throw e;
        }
    }
}
