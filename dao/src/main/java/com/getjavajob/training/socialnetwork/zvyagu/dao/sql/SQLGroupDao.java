package com.getjavajob.training.socialnetwork.zvyagu.dao.sql;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.Group;
import com.getjavajob.training.socialnetwork.zvyagu.dao.GroupDao;
import com.getjavajob.training.socialnetwork.zvyagu.dao.exeptions.DaoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

@Repository("groupDao")
public class SQLGroupDao extends SQLAbstractDao<Group> implements GroupDao {
    private static final Logger logger = LoggerFactory.getLogger(SQLGroupDao.class);

    @Autowired
    public SQLGroupDao() {
        super();
    }

    @Override
    public Set<Group> search(String[] words, int limit, int offset) throws DaoException {
        logger.debug("searching groups by words {} with limit {} and offset {}", Arrays.toString(words), limit, offset);
        EntityManager manager = getEntityManager();
        CriteriaBuilder builder = manager.getCriteriaBuilder();
        CriteriaQuery<Group> query = builder.createQuery(Group.class);
        Root<Group> root = query.from(Group.class);
        query.select(root);

        searchPredicate(words, builder, query, root);

        return manager.createQuery(query).setMaxResults(limit).setFirstResult(offset).getResultList().stream().collect(Collectors.toSet());
    }

    @Override
    public long searchCount(String[] words) throws DaoException {
        logger.debug("counting groups from search by words {}", Arrays.toString(words));
        EntityManager manager = getEntityManager();
        CriteriaBuilder builder = manager.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<Group> root = query.from(Group.class);
        query.select(builder.count(root));
        searchPredicate(words, builder, query, root);
        return manager.createQuery(query).getSingleResult();
    }

    private void searchPredicate(String[] words, CriteriaBuilder builder, CriteriaQuery<?> query, Root<Group> root) {
        if (words != null && words.length > 0) {
            Predicate wherePredicate = builder.conjunction();
            for (int i = 0; i < words.length; i++) {
                wherePredicate = builder.and(wherePredicate, builder.like(root.get("name"), '%' + words[i] + '%'));
            }
            query.where(wherePredicate);
        }
    }

    @Override
    public Set<Group> getGroupsByOwnerID(long id) throws DaoException {
        logger.debug("getting groups by owner id {}", id);
        try {
            EntityManager manager = getEntityManager();
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<Group> query = builder.createQuery(Group.class);
            Root<Group> root = query.from(Group.class);
            query.where(builder.equal(root.get("owner"), id));
            return manager.createQuery(query).getResultList().stream().collect(Collectors.toSet());
        } catch (Exception e) {
            logger.error("error getting groups by owner id {}", id, e);
            throw new DaoException(e);
        }
    }

    @Override
    public Set<Account> getMembers(Group group) throws DaoException {
        return validateEntity(group).getMembers();
    }

    @Override
    public Set<Account> getMembers(Group group, int limit, int offset) throws DaoException {
        try {
            EntityManager manager = getEntityManager();
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<Account> query = builder.createQuery(Account.class);
            Root<Group> groupRoot = query.from(Group.class);
            query.where(builder.equal(groupRoot.get("id"), group.getID()));
            SetJoin<Group, Account> join = groupRoot.joinSet("members");
            CriteriaQuery<Account> q = query.select(join);
            return manager.createQuery(q).setMaxResults(limit).setFirstResult(offset).getResultList().stream().collect(Collectors.toSet());
        } catch (Exception e) {
            logger.error("error getting members ", e);
            throw new DaoException(e);
        }
    }

    @Override
    public Set<Group> getAccountGroups(Account account) throws DaoException {
        try {
            EntityManager manager = getEntityManager();
            TypedQuery<Group> query = manager.createNamedQuery("Group.getAccountGroups", Group.class);
            return query.setParameter("acc", account).getResultList().stream().collect(Collectors.toSet());
        } catch (Exception e) {
            logger.error("error getting groups for account {}", account.getID(), e);
            throw new DaoException(e);
        }
    }

    @Override
    public Set<Group> getAccountGroups(Account account, int limit, int offset) throws DaoException {
        try {
            EntityManager manager = getEntityManager();
            TypedQuery<Group> query = manager.createNamedQuery("Group.getAccountGroups", Group.class);
            return query.setParameter("acc", account).setMaxResults(limit).setFirstResult(offset).getResultList().stream().collect(Collectors.toSet());
        } catch (Exception e) {
            logger.error("error getting groups for account {}", account.getID(), e);
            throw new DaoException(e);
        }
    }

    @Override
    protected Class<Group> getEntityClass() {
        return Group.class;
    }
}
