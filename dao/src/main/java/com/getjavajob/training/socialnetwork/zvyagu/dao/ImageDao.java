package com.getjavajob.training.socialnetwork.zvyagu.dao;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.Group;
import com.getjavajob.training.socialnetwork.zvyagu.common.Image;
import com.getjavajob.training.socialnetwork.zvyagu.dao.exeptions.DaoException;

public interface ImageDao extends Dao<Image> {
//    void deleteAccountRelations(long accountID) throws DaoException;

//    void deleteGroupRelations(long groupID) throws DaoException;

    /**
     * For creation and update
     */
    void createAccountImage(Account account, Image image) throws DaoException;

    Image getAccountImage(Account account) throws DaoException;

    void deleteAccountImage(Account account) throws DaoException;

    void createGroupImage(Group account, Image image) throws DaoException;

    Image getGroupImage(Group account) throws DaoException;

    void deleteGroupImage(Group account) throws DaoException;
}
