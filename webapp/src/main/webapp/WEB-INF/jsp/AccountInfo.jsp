<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-list"></span>
        Person information
    </div>
    <div class="panel-body">
        <div class="row row__item">
            <label class="col-md-4">Last name:</label>
            <div class="col-md-8">${account.lastName}</div>
        </div>
        <div class="row row__item">
            <label class="col-md-4">First name:</label>
            <div class="col-md-8">${account.firstName}</div>
        </div>
        <div class="row row__item">
            <label class="col-md-4">Middle name:</label>
            <div class="col-md-8">${account.middleName}</div>
        </div>
        <div class="row row__item">
            <label class="col-md-4">Registration date:</label>
            <fmt:formatDate var="registration" value="${account.registrationExactlyDate}" pattern="yyyy.MM.dd"/>
            <div class="col-md-8">${registration}</div>
        </div>
        <div class="row row__item">
            <label class="col-md-4">Birthday:</label>
            <fmt:formatDate var="birthday" value="${account.birthdayDate}" pattern="yyyy.MM.dd"/>
            <div class="col-md-8">${birthday}</div>
        </div>
        <div class="row row__item">
            <label class="col-md-4">Phones:</label>
            <div class="col-md-8">${fn:join(account.phones.toArray(), ", ")}</div>
        </div>
        <div class="row row__item">
            <label class="col-md-4">Work phones:</label>
            <div class="col-md-8">${fn:join(account.workPhones.toArray(), ", ")}</div>
        </div>
        <div class="row row__item">
            <label class="col-md-4">Address:</label>
            <div class="col-md-8">${account.address}</div>
        </div>
        <div class="row row__item">
            <label class="col-md-4">Work address:</label>
            <div class="col-md-8">${account.workAddress}</div>
        </div>
        <div class="row row__item">
            <label class="col-md-4">ICQ:</label>
            <div class="col-md-8">${account.icq}</div>
        </div>
        <div class="row row__item">
            <label class="col-md-4">Skype:</label>
            <div class="col-md-8">${account.skype}</div>
        </div>
        <div class="row row__item">
            <label class="col-md-4">About me:</label>
            <div class="col-md-8">${account.extraInfo.replaceAll('\\n', '</br>')}</div>
        </div>
        <%--<c:if test="${account.equals(current)}">--%>
        <div class="div__align-right">
            <form action="${pageContext.request.contextPath}${uri}/edit" method="get">
                <button type="submit" class="btn btn-default">Edit person information</button>
            </form>
        </div>
        <%--</c:if>--%>
    </div>
</div>