package com.getjavajob.training.socialnetwork.zvyagu.dao;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.Group;
import com.getjavajob.training.socialnetwork.zvyagu.dao.exeptions.DaoException;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

public interface GroupDao extends Dao<Group>, SearchDao<Group> {
//    @Transactional
//    void deleteAccountRelations(long accountID) throws DaoException;

    default Set<Group> getGroupsByOwner(Account owner) throws DaoException {
        return getGroupsByOwnerID(owner.getID());
    }

    Set<Group> getGroupsByOwnerID(long id) throws DaoException;

    Set<Account> getMembers(Group group) throws DaoException;

    Set<Account> getMembers(Group group, int limit, int offset) throws DaoException;

    Set<Group> getAccountGroups(Account account) throws DaoException;

    Set<Group> getAccountGroups(Account account, int limit, int offset) throws DaoException;
}
