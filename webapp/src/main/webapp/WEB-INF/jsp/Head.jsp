<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.js"></script>
    <script src="${pageContext.request.contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.js"></script>
    <script src="${pageContext.request.contextPath}/webjars/jquery.inputmask/3.3.1/inputmask/inputmask.js"></script>
    <script src="${pageContext.request.contextPath}/webjars/jquery.inputmask/3.3.1/inputmask/jquery.inputmask.js"></script>
    <script src="${pageContext.request.contextPath}/webjars/bootstrap/3.3.7/js/bootstrap.js"></script>
    <%--<script src="${pageContext.request.contextPath}/webjars/parsleyjs/2.1.2/parsley.js"></script>--%>
    <script src="${pageContext.request.contextPath}/resources/js/lib/datepicker-ru.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/lib/parsley.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.structure.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.theme.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/webjars/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/webjars/parsleyjs/2.1.2/parsley.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/lib/parsley.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/main.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/input-form.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/menu.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/file-upload.css">
    <title>social network</title>
</head>
