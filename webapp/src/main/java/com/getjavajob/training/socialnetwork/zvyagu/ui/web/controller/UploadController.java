package com.getjavajob.training.socialnetwork.zvyagu.ui.web.controller;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.Group;
import com.getjavajob.training.socialnetwork.zvyagu.common.Image;
import com.getjavajob.training.socialnetwork.zvyagu.service.Service;
import com.getjavajob.training.socialnetwork.zvyagu.ui.web.Attribute;
import com.getjavajob.training.socialnetwork.zvyagu.ui.web.Uri;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@Controller("uploadController")
@RequestMapping(Uri.CONTENT_UPLOAD)
public class UploadController {
    private static final Logger logger = LoggerFactory.getLogger(UploadController.class);
    @Autowired
    private Service service;

    @RequestMapping(value = Uri.ACCOUNT + "/{id}", method = RequestMethod.POST)
    public String uploadAccountImage(@PathVariable("id") long id,
                                     @RequestParam(Attribute.IMAGE) MultipartFile file,
                                     @RequestParam(Attribute.REDIRECT_URI) String redirect) throws Exception {
        try {
            Account account = service.getAccountService().getAccount(id);
            service.getAccountService().createAccountImage(account, prepareImage(file));
            return "redirect:" + redirect;
        } catch (Exception e) {
            logger.error("error uploading image for account {}", id);
            throw e;
        }
    }

    @RequestMapping(value = Uri.GROUP + "/{id}", method = RequestMethod.POST)
    public String uploadGroupImage(@PathVariable("id") long id,
                                   @RequestParam(Attribute.IMAGE) MultipartFile file,
                                   @RequestParam(Attribute.REDIRECT_URI) String redirect) throws Exception {
        try {
            Group group = service.getGroupService().getGroup(id);
            service.getGroupService().createGroupImage(group, prepareImage(file));
            return "redirect:" + redirect;
        } catch (Exception e) {
            logger.error("error uploading image for group {}", id, e);
            throw e;
        }
    }

    private Image prepareImage(MultipartFile file) throws IOException {
        String fileName = file.getOriginalFilename();
        int index = fileName.lastIndexOf('.');
        String ext = index == -1 ? null : fileName.substring(index + 1);
        InputStream fileContent = file.getInputStream();
        Image image = service.newImage();
        image.setName(fileName);
        image.setFileExtension(ext);
        image.setContent(fileContent);
        return image;
    }
}
