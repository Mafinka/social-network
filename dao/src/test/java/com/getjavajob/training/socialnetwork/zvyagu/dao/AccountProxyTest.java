package com.getjavajob.training.socialnetwork.zvyagu.dao;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.dao.exeptions.DaoException;
import com.getjavajob.training.socialnetwork.zvyagu.dao.sql.SQLDaoService;
import org.h2.tools.RunScript;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.io.BufferedInputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;

import static com.getjavajob.training.socialnetwork.zvyagu.dao.sql.SQLDaoTestUtil.DATABASE_INIT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:dao-context-override.xml"})
public class AccountProxyTest {
    @Autowired
    private DaoFactory factory;
    @Autowired
    private DaoService service;
    @Autowired
    private DataSource dataSource;

    @Before
    public void init() throws SQLException, InterruptedException, DaoException {
        try (Connection connection = dataSource.getConnection()) {
            RunScript.execute(connection, new InputStreamReader(
                    new BufferedInputStream(ClassLoader.getSystemResourceAsStream(DATABASE_INIT))));
        }
//        factory = service.getFactory();
    }

    @After
    public void free() throws Exception {
        service.close();
    }

    @Test
    public void testEquals() throws Exception {
        Account account = factory.getAccountDao().get(1);
        Account proxy = new AccountProxy(1, service);
        assertEquals(account.hashCode(), proxy.hashCode());
        assertEquals(account, proxy);
        assertEquals(proxy, account);
    }

    @Test
    public void testCollectionsNotLoad() throws DaoException {
        AccountProxy proxy1 = new AccountProxy(1, service);
        AccountProxy proxy2 = new AccountProxy(2, service);
        new HashSet<>(Arrays.asList(proxy1, proxy2));
        assertFalse(proxy1.isLoaded());
        assertFalse(proxy2.isLoaded());
    }
}
