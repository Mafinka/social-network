package com.getjavajob.training.socialnetwork.zvyagu.ui.web.controller;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.Group;
import com.getjavajob.training.socialnetwork.zvyagu.service.Service;
import com.getjavajob.training.socialnetwork.zvyagu.ui.web.Attribute;
import com.getjavajob.training.socialnetwork.zvyagu.ui.web.Page;
import com.getjavajob.training.socialnetwork.zvyagu.ui.web.Uri;
import com.getjavajob.training.socialnetwork.zvyagu.ui.web.wrap.SearchDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.*;
import java.util.stream.Collectors;

import static com.getjavajob.training.socialnetwork.zvyagu.ui.web.ServletUtil.deleteLoginCookies;
import static com.getjavajob.training.socialnetwork.zvyagu.ui.web.ServletUtil.saveLoginCookies;

@Controller("mainController")
public class MainController {
    private static final Logger logger = LoggerFactory.getLogger(MainController.class);
    private static final int SEARCH_LIMIT = 10;

    @Autowired
    private Service service;
    @Value("#{servletContext.contextPath}")
    private String contextPath;

    @RequestMapping(Uri.DEF)
    public String defRequest() {
        return "redirect:" + Uri.HOME;
    }

    @RequestMapping(value = Uri.LOGIN, method = RequestMethod.GET)
    public String getLoginPage(@SessionAttribute(value = Attribute.ACCOUNT_ID, required = false) Long accountID,
                               Model model,
                               @CookieValue(value = Attribute.EMAIL, required = false) String email,
                               @CookieValue(value = Attribute.PASSWORD, required = false) String password) throws Exception {
        try {
            if (accountID != null) {
                return "redirect:" + Uri.HOME;
            } else {
                prepareLoginModel(model, email, password, StringUtils.hasText(email) && StringUtils.hasText(password), false);
                return Page.LOGIN;
            }
        } catch (Exception e) {
            logger.error("error getting login page", e);
            throw e;
        }
    }

    @RequestMapping(value = Uri.LOGIN, method = RequestMethod.POST)
    public String tryLogin(HttpSession session,
                           HttpServletResponse resp,
                           Model model,
                           @RequestParam(Attribute.EMAIL) String email,
                           @RequestParam(Attribute.PASSWORD) String password,
                           @RequestParam(value = Attribute.SAVE_LOGIN_COOKIES, required = false) Object save) throws Exception {
        try {
            if (login(session, email, password)) {
                if (hasSaveFlag(save)) {
                    saveLoginCookies(resp, email, password);
                }
                return "redirect:" + Uri.HOME;
            } else {
                prepareLoginModel(model, email, null, save != null, true);
                return Page.LOGIN;
            }
        } catch (Exception e) {
            logger.error("error trying to login by email {} and password {}", email, password, e);
            throw e;
        }
    }

    @RequestMapping(value = Uri.LOGOUT, method = RequestMethod.POST)
    public String logout(HttpServletResponse resp, HttpSession session) {
        try {
            Long id = (Long) session.getAttribute(Attribute.ACCOUNT_ID);
            session.invalidate();
            deleteLoginCookies(resp);
            logger.info("logout for user {} successful", id);
            return "redirect:" + Uri.LOGIN;
        } catch (Exception e) {
            logger.error("error logout", e);
            throw e;
        }
    }

    @RequestMapping(value = Uri.SEARCH, method = RequestMethod.GET)
    public String getSearchPage(@RequestParam(value = Attribute.SEARCH_WORDS, required = false) String words,
                                Model model) throws Exception {
        model.addAttribute(Attribute.SEARCH_WORDS, words);
        model.addAttribute(Attribute.ACCOUNTS_PAGES, (service.getAccountService().searchCount(words) - 1) / SEARCH_LIMIT + 1);
        model.addAttribute(Attribute.ACCOUNTS, service.getAccountService().search(words, SEARCH_LIMIT, 0));
        model.addAttribute(Attribute.GROUPS_PAGES, (service.getGroupService().searchCount(words) - 1) / SEARCH_LIMIT + 1);
        model.addAttribute(Attribute.GROUPS, service.getGroupService().search(words, SEARCH_LIMIT, 0));
        return Page.SEARCH;
    }

    @RequestMapping(value = Uri.SEARCH + "/accounts/{page}", method = RequestMethod.POST)
    public String getAccountsSearch(@PathVariable(value = "page") int page,
                                    @RequestParam(value = "words") String words,
                                    Model model) throws Exception {
        int offset = (page - 1) * SEARCH_LIMIT;
        model.addAttribute(Attribute.ACCOUNTS, service.getAccountService().search(words, SEARCH_LIMIT, offset));
        return Page.ACCOUNT_SEARCH_PAGE;
    }

    @RequestMapping(value = Uri.SEARCH + "/groups/{page}", method = RequestMethod.POST)
    public String getGroupsSearch(@PathVariable(value = "page") int page,
                                  @RequestParam(value = "words") String words,
                                  Model model) throws Exception {
        int offset = (page - 1) * SEARCH_LIMIT;
        model.addAttribute(Attribute.GROUPS, service.getGroupService().search(words, SEARCH_LIMIT, offset));
        return Page.GROUP_SEARCH_PAGE;
    }

    @RequestMapping(value = Uri.SEARCH + "/auto", method = RequestMethod.GET)
    @ResponseBody
    public List<SearchDTO> getSearchAuto(@RequestParam(Attribute.SEARCH_WORDS) String words, Model model) throws Exception {
        model.addAttribute(Attribute.SEARCH_WORDS, words);
        List<SearchDTO> result = new ArrayList<>();
        result.addAll(service.getAccountService().search(words).stream().map(account -> new SearchDTO(account,
                contextPath + Uri.ACCOUNT + '/' + account.getID())).collect(Collectors.toList()));
        result.addAll(service.getGroupService().search(words).stream().map(group -> new SearchDTO(group,
                contextPath + Uri.GROUP + '/' + group.getID())).collect(Collectors.toList()));
        return result;
    }

    private void prepareLoginModel(Model model, String email, String password, boolean saveFlag, boolean failedFlag) {
        addEmail(model, email);
        if (saveFlag) {
            addSaveFlag(model);
        }
        if (failedFlag) {
            addFailedFlag(model);
        }
        addPassword(model, password);
    }

    private boolean hasSaveFlag(Object save) {
        return save != null;
    }

    private boolean login(HttpSession session, String email, String password) throws Exception {
        Account account = service.getAccountService().getAccount(email, password);
        System.out.println("login account = " + account);
        if (account != null) {
            logger.debug("login for email '{}' successful, current account {}", email, account.getID());
            session.setAttribute(Attribute.ACCOUNT_ID, account.getID());
            return true;
        } else {
            logger.debug("trying login for email {} failed", email);
            return false;
        }
    }

    private void addEmail(Model model, String email) {
        model.addAttribute(Attribute.EMAIL, email);
    }

    private void addPassword(Model model, String password) {
        model.addAttribute(Attribute.PASSWORD, password);
    }

    private void addSaveFlag(Model model) {
        model.addAttribute(Attribute.SAVE_LOGIN_COOKIES, true);
    }

    private void addFailedFlag(Model model) {
        model.addAttribute(Attribute.LOGIN_FAILED, true);
    }
}
