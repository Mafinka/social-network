package com.getjavajob.training.socialnetwork.zvyagu.service.impl;

import com.getjavajob.training.socialnetwork.zvyagu.common.Group;
import com.getjavajob.training.socialnetwork.zvyagu.dao.GroupDao;
import com.getjavajob.training.socialnetwork.zvyagu.dao.ImageDao;
import com.getjavajob.training.socialnetwork.zvyagu.service.GroupService;
import org.junit.Before;
import org.junit.Test;

import static com.getjavajob.training.socialnetwork.zvyagu.service.impl.ServiceTestUtil.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class GroupServiceTest {
    private GroupDao groupDao;
    private ImageDao imageDao;
    private GroupService service;

    @Before
    public void init() throws Exception {
        groupDao = mock(GroupDao.class);
        imageDao = mock(ImageDao.class);
        service = new GroupServiceImpl(groupDao, imageDao);
    }

    @Test
    public void createGroup() throws Exception {
        Group entity = getFirstGroup();
        service.createGroup(entity);
        verify(groupDao).create(entity);
    }

    @Test
    public void getGroup() throws Exception {
        service.getGroup(1);
        verify(groupDao).get(1);
    }

    @Test
    public void updateGroup() throws Exception {
        Group entity = getSecondGroup();
        entity.setID(getFirstGroup().getID());
        service.updateGroup(entity);
        verify(groupDao).update(entity);
    }

    @Test
    public void deleteGroup() throws Exception {
        service.deleteGroup(1L);
        verify(groupDao).delete(1L);
    }

    @Test
    public void getAll() throws Exception {
        service.getAllGroups();
        verify(groupDao).getAll();
    }

    @Test
    public void getImage() throws Exception {
        service.getGroupImage(getFirstGroup());
        verify(imageDao).getGroupImage(any());
    }

    @Test
    public void createImage() throws Exception {
        service.createGroupImage(getFirstGroup(), getFirstImage());
        verify(imageDao).createGroupImage(any(), any());
    }

    @Test
    public void deleteImage() throws Exception {
        service.deleteGroupImage(getFirstGroup());
        verify(imageDao).deleteGroupImage(any());
    }

    @Test
    public void search() throws Exception {
        service.search("abc");
        verify(groupDao).search(new String[]{"abc"});
    }
}
