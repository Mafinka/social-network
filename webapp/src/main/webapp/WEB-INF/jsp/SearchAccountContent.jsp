<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div page="${accountsPage}" class="row accounts-content">
    <c:forEach var="current" items="${accounts}">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="media">
                        <div class="media-left">
                            <a href="${pageContext.request.contextPath}/account/${current.ID}">
                                <img class="min-photo img-thumbnail"
                                     src="${pageContext.request.contextPath}/content/account/${current.ID}"
                                     onerror="this.src='${pageContext.request.contextPath}/resources/images/NotFound.png'">
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="input-form__line">
                                <div class="input-form__label-70 div__top">Full name:</div>
                                <a class="link" href="${pageContext.request.contextPath}/account/${current.ID}">
                                        ${current.lastName} ${current.firstName} ${current.middleName}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </c:forEach>
</div>