<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="Common.jsp" %>
<html>
<%@include file="Head.jsp" %>
<%--@elvariable id="group" type="com.getjavajob.training.socialnetwork.zvyagu.common.Group"--%>
<body>
<%@include file="MenuTop.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <%@include file="MenuNavigate.jsp" %>
        </div>
        <div class="col-md-7">
            <%@include file="GroupInfo.jsp" %>
            <%@include file="AccountMessages.jsp" %>
        </div>
        <div class="col-md-3">
            <c:set var="entity" value="group"/>
            <c:set var="id" value="${group.ID}"/>
            <c:set var="redirect" value="${uri}"/>
            <%@include file="PhotoUpload.jsp" %>
            <%@include file="MenuGroupMembers.jsp" %>
        </div>
    </div>
</div>
</body>
<%@include file="Footer.jsp"%>
</html>
