package com.getjavajob.training.socialnetwork.zvyagu.common.constant;

public enum MessageType {
    PRIVATE,
    ACCOUNT,
    GROUP
}
