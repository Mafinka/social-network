package com.getjavajob.training.socialnetwork.zvyagu.ui.web.controller;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.Group;
import com.getjavajob.training.socialnetwork.zvyagu.common.Image;
import com.getjavajob.training.socialnetwork.zvyagu.service.Service;
import com.getjavajob.training.socialnetwork.zvyagu.ui.web.Uri;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletContext;

@Controller("contentController")
@RequestMapping(Uri.CONTENT_LOAD)
public class ContentController {
    private static final Logger logger = LoggerFactory.getLogger(ContentController.class);
    @Autowired
    private Service service;
    @Autowired
    private ServletContext servletContext;

    @RequestMapping(value = Uri.ACCOUNT + "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> getAccountImage(@PathVariable("id") long id) throws Exception {
        try {
            Account account = service.getAccountService().getAccount(id);
            Image image = service.getAccountService().getAccountImage(account);
            return getImageResponse(image);
        } catch (Exception e) {
            logger.error("error getting image for account {}", id, e);
            throw e;
        }
    }

    @RequestMapping(value = Uri.GROUP + "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> getGroupImage(@PathVariable("id") long id) throws Exception {
        try {
            Group group = service.getGroupService().getGroup(id);
            Image image = service.getGroupService().getGroupImage(group);
            return getImageResponse(image);
        } catch (Exception e) {
            logger.error("error getting image for group {}", id, e);
            throw e;
        }
    }

    private ResponseEntity<byte[]> getImageResponse(Image image) throws Exception {
        HttpHeaders headers = new HttpHeaders();
        if (image != null) {
            headers.setCacheControl(CacheControl.noCache().getHeaderValue());
            byte[] data = IOUtils.toByteArray(image.getContent());
            headers.setContentType(MediaType.valueOf(servletContext.getMimeType(image.getName())));
            return new ResponseEntity<>(data, headers, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, headers, HttpStatus.NO_CONTENT);
        }
    }
}
