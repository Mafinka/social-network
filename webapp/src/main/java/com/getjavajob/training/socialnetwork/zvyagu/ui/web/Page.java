package com.getjavajob.training.socialnetwork.zvyagu.ui.web;

public abstract class Page {
    public static final String LOGIN = "PageLogin";
    public static final String HOME = "PageAccount";
    public static final String ACCOUNT = "PageAccount";
    public static final String GROUP = "PageGroup";
    public static final String REGISTRATION = "PageRegistration";
    public static final String SEARCH = "PageSearch";

    public static final String ACCOUNT_SEARCH_PAGE = "SearchAccountContent";
    public static final String GROUP_SEARCH_PAGE = "SearchGroupContent";
    public static final String ACCOUNT_EDIT_FORM = "AccountEdit";
}
