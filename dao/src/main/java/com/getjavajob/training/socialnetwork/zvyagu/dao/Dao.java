package com.getjavajob.training.socialnetwork.zvyagu.dao;

import com.getjavajob.training.socialnetwork.zvyagu.common.BaseEntity;
import com.getjavajob.training.socialnetwork.zvyagu.dao.exeptions.DaoException;

import java.util.Set;

public interface Dao<E extends BaseEntity> {
    void create(E entity) throws DaoException;

    E get(long id) throws DaoException;

    Set<E> getAll() throws DaoException;

    void update(E entity) throws DaoException;

    void delete(long id) throws DaoException;
}
