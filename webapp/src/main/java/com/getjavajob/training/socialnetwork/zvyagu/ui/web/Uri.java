package com.getjavajob.training.socialnetwork.zvyagu.ui.web;

/**
 * Abstract class for all requests. Not enum, because java not load enu before annotations
 */
public abstract class Uri {
    // pages
    public static final String DEF = "/";
    public static final String GROUP = "/group";
    public static final String ACCOUNT = "/account";

    public static final String HOME = ACCOUNT + UriAccount.HOME;
    public static final String REGISTRATION = ACCOUNT + UriAccount.REGISTRATION;
    public static final String LOGIN = "/login";
    public static final String LOGOUT = "/logout";
    public static final String SEARCH = "/search";
    public static final String TEST = "/test";

    // processes
    public static final String CREATE_ACCOUNT = Uri.ACCOUNT + UriAccount.REGISTRATION + UriAccount.UPDATE;
    public static final String CONTENT_LOAD = "/content";
    public static final String CONTENT_UPLOAD = "/upload";
}
