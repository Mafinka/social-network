package com.getjavajob.training.socialnetwork.zvyagu.common;

import com.getjavajob.training.socialnetwork.zvyagu.common.constant.FriendCategory;

import javax.persistence.*;
import javax.persistence.Entity;

@Entity
@Table(name = "FRIENDS")
public class Friend extends BaseEntity {
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ACCOUNT_ID", referencedColumnName = "ID")
    private Account owner;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "FRIEND_ID", referencedColumnName = "ID")
    private Account account;
    @Enumerated(EnumType.STRING)
    @Column(name = "CATEGORY")
    private FriendCategory category;

    public Friend() { }

    public Friend(Account owner, Account account) {
        this(owner, account, FriendCategory.FRIEND);
    }

    public Friend(Account owner, Account account, FriendCategory category) {
        this.owner = owner;
        this.account = account;
        this.category = category;
    }

    public Account getOwner() {
        return owner;
    }

    public void setOwner(Account owner) {
        this.owner = owner;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public FriendCategory getCategory() {
        return category;
    }

    public void setCategory(FriendCategory category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Friend friend = (Friend) o;

        return account != null ? account.equals(friend.account) : friend.account == null;
    }

    @Override
    public int hashCode() {
        return account != null ? account.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Friend{" +
                "account=" + account +
                ", category=" + category +
                '}';
    }
}
