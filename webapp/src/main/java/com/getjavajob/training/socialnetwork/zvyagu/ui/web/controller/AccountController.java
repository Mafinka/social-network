package com.getjavajob.training.socialnetwork.zvyagu.ui.web.controller;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.service.AccountService;
import com.getjavajob.training.socialnetwork.zvyagu.service.GroupService;
import com.getjavajob.training.socialnetwork.zvyagu.service.Service;
import com.getjavajob.training.socialnetwork.zvyagu.ui.web.*;
import com.getjavajob.training.socialnetwork.zvyagu.ui.web.propertyeditors.CustomCalendarEditor;
import com.getjavajob.training.socialnetwork.zvyagu.ui.web.wrap.AccountWrapper;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@Controller("accountController")
@RequestMapping(Uri.ACCOUNT)
public class AccountController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
    private static final int FRIENDS_LIST_LIMIT = 6;
    private static final int GROUPS_LIST_LIMIT = 6;

    private DateFormat df = new SimpleDateFormat("yyyy.MM.dd");
    @Autowired
    private Service service;
    @Autowired
    private AccountService accountService;
    @Autowired
    private GroupService groupService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        try {
            logger.debug("initBinder() initiating binders");
            binder.registerCustomEditor(Calendar.class, new CustomCalendarEditor(df));
        } catch (Exception e) {
            logger.error("error initiating binders", e);
            throw e;
        }
    }

    @RequestMapping(value = UriAccount.HOME, method = RequestMethod.GET)
    public String getHomePage(@RequestAttribute(Attribute.CURRENT_ACCOUNT) Account account,
                              Model model) throws Exception {
        try {
            model.addAttribute(AccountPageContent.ATTR, AccountPageContent.INFO);
            fillAccountPageModel(model, account);
            return Page.HOME;
        } catch (Exception e) {
            logger.error("error getting home page", e);
            throw e;
        }
    }

    @RequestMapping(value = UriAccount.HOME + UriAccount.EDIT, method = RequestMethod.GET)
    public String getUpdateHomePage(@RequestAttribute(Attribute.CURRENT_ACCOUNT) Account account,
                                    Model model) throws Exception {
        try {
            model.addAttribute(AccountPageContent.ATTR, AccountPageContent.EDIT);
            fillAccountPageModel(model, account);
            return Page.HOME;
        } catch (Exception e) {
            logger.error("error getting edit current account page", e);
            throw e;
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String getAccountPage(@PathVariable(value = "id", required = false) Long id, Model model,
                                 @SessionAttribute(Attribute.ACCOUNT_ID) long currentID) throws Exception {
        try {
            if (id == null || id == currentID) {
                return "redirect:" + Uri.HOME;
            } else {
                Account account = getAccount(id);
                model.addAttribute(AccountPageContent.ATTR, AccountPageContent.INFO);
                fillAccountPageModel(model, account);
                return Page.ACCOUNT;
            }
        } catch (Exception e) {
            logger.error("error getting account page", e);
            throw e;
        }
    }

    @RequestMapping(value = "/{id}" + UriAccount.EDIT, method = RequestMethod.GET)
    public String getUpdateAccountPage(@PathVariable(value = "id", required = false) Long id, Model model,
                                       @SessionAttribute(Attribute.ACCOUNT_ID) long currentID) throws Exception {
        try {
            if (id == null || id == currentID) {
                return "redirect:" + Uri.HOME + UriAccount.EDIT;
            } else {
                Account account = getAccount(id);
                model.addAttribute(AccountPageContent.ATTR, AccountPageContent.EDIT);
                fillAccountPageModel(model, account);
                return Page.ACCOUNT;
            }
        } catch (Exception e) {
            logger.error("error getting update account page", e);
            throw e;
        }
    }

    private void fillAccountPageModel(Model model, Account account) throws Exception {
        model.addAttribute(Attribute.ACCOUNT, account);
        model.addAttribute(Attribute.FRIENDS, accountService.getFriends(account, FRIENDS_LIST_LIMIT, 0));
        model.addAttribute(Attribute.GROUPS, groupService.getAccountGroups(account, GROUPS_LIST_LIMIT, 0));
    }

    @RequestMapping(value = UriAccount.REGISTRATION, method = RequestMethod.GET)
    public String getRegistrationPage(Model model) {
        try {
            model.addAttribute(Attribute.ACCOUNT, service.getAccountService().newAccount());
            return Page.REGISTRATION;
        } catch (Exception e) {
            logger.error("error getting registration page", e);
            throw e;
        }
    }

    @RequestMapping(value = UriAccount.HOME + UriAccount.EDIT + UriAccount.UPDATE, method = RequestMethod.POST)
    public String updateCurrentAccount(@SessionAttribute(Attribute.ACCOUNT_ID) Long id,
                                       @ModelAttribute AccountWrapper wrapper) throws Exception {
        try {
            return updateAccount(id, wrapper);
        } catch (Exception e) {
            logger.error("error updating current account", e);
            throw e;
        }
    }

    @RequestMapping(value = UriAccount.REGISTRATION + UriAccount.UPDATE, method = RequestMethod.POST)
    public String createAccount(@ModelAttribute AccountWrapper wrapper) throws Exception {
        try {
            Account account = service.getAccountService().newAccount();
            wrapper.setAccount(account);
            service.getAccountService().createAccount(wrapper.getAccount());
            return "redirect:" + Uri.LOGIN;
        } catch (Exception e) {
            logger.error("error creating account {}", wrapper.getAccount(), e);
            throw e;
        }
    }

    @RequestMapping(value = "/{id}" + UriAccount.EDIT + UriAccount.UPDATE, method = RequestMethod.POST)
    public String updateAccount(@PathVariable("id") Long id,
                                @ModelAttribute AccountWrapper wrapper) throws Exception {
        try {
            wrapper.setID(id);
            wrapper.setAccount(service.getAccountService().getAccount(id));
            service.getAccountService().updateAccount(wrapper.getAccount());
            return "redirect:" + Uri.ACCOUNT + '/' + id;
        } catch (Exception e) {
            logger.error("error updating account by id", id, e);
            throw e;
        }
    }

    @RequestMapping(value = UriAccount.HOME + UriAccount.EDIT + UriAccount.XML_UPLOAD, method = RequestMethod.POST)
    public String getCurrentAccountFormFromXML(@SessionAttribute(Attribute.ACCOUNT_ID) Long id,
                                               @RequestParam(value = "xml") MultipartFile file,
                                               Model model) throws Exception {
        return getAccountFormFromXML(id, file, model);
    }

    @RequestMapping(value = "/{id}" + UriAccount.EDIT + UriAccount.XML_UPLOAD, method = RequestMethod.POST)
    public String getAccountFormFromXML(@PathVariable("id") Long id,
                                        @RequestParam(value = "xml") MultipartFile file,
                                        Model model) throws Exception {
        model.addAttribute("edit_account_label", "Edit person information");
        Account account = service.getAccountService().fromXml(IOUtils.toString(file.getInputStream(), "UTF-8"));
        account.setID(id);
        model.addAttribute(Attribute.ACCOUNT, account);
        return Page.ACCOUNT_EDIT_FORM;
    }

    @RequestMapping(value = UriAccount.HOME + UriAccount.EDIT + UriAccount.XML_LOAD, method = RequestMethod.GET)
    @ResponseBody
    public byte[] getXmlFile(@RequestAttribute(value = Attribute.CURRENT_ACCOUNT) Account account) throws Exception {
        return service.getAccountService().toXml(account).getBytes();
    }

    @RequestMapping(value = "/{id}" + UriAccount.EDIT + UriAccount.XML_LOAD, method = RequestMethod.GET)
    @ResponseBody
    public byte[] getXmlFile(@PathVariable("id") Long id) throws Exception {
        Account account = service.getAccountService().getAccount(id);
        return service.getAccountService().toXml(account).getBytes();
    }

    private Account getAccount(long id) throws Exception {
        return service.getAccountService().getAccount(id);
    }
}
