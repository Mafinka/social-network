package com.getjavajob.training.socialnetwork.zvyagu.dao;

public abstract class AbstractFactory implements DaoFactory {
    private DaoService daoService;

    public AbstractFactory(DaoService daoService) {
        this.daoService = daoService;
    }

    protected DaoService getService() {
        return daoService;
    }
}
