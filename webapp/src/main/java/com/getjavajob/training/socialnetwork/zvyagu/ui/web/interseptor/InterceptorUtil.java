package com.getjavajob.training.socialnetwork.zvyagu.ui.web.interseptor;

import com.getjavajob.training.socialnetwork.zvyagu.ui.web.Uri;

public abstract class InterceptorUtil {
    public static final String RESOURCES_REG = "^/resources/.+|^/webjars/.+|^/content/.+";
    public static final String LOGIN_FILTER_REG = RESOURCES_REG
            + "|^" + Uri.REGISTRATION + "$|^" + Uri.LOGIN + "$|^" + Uri.CREATE_ACCOUNT + "$";
}
