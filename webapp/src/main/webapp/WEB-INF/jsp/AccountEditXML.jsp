<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-open-file"></span>
        I/O account info
    </div>
    <div class="panel-body">
        <form class="form" action="" method="post">
            <div class="file_upload xml_upload">
                <button type="button" class="btn btn-info btn-block">Load from xml</button>
                <div hidden></div>
                <input name="xml" type="file" data-page-uri="${uri}">
            </div>
        </form>
        <div class="div__spacing"></div>
        <a download="account.xml" href="${uri}/to_xml" class="btn btn-default btn-block">Download xml</a>
    </div>
</div>