<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--@elvariable id="current" type="com.getjavajob.training.socialnetwork.zvyagu.common.Account"--%>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <p class="navbar-text">${current.email}</p>
            <c:if test="${isAdmin}">
                <p class="navbar-text">Administrator</p>
            </c:if>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <form class="navbar-form navbar-right" role="search" action="${pageContext.request.contextPath}/search" method="get">
                <div class="form-group">
                    <div class="input-group">
                        <input name="words" type="search" class="form-control top-menu__search-input" placeholder="Search" value="${words}">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default">&nbsp;<span class="glyphicon glyphicon-search"></span>&nbsp;</button>
                        </span>
                    </div>
                </div>
            </form>
            <form class="navbar-form navbar-right" action="${pageContext.request.contextPath}/logout" method="post" role="form">
                <div class="form-group">
                    <button type="submit" class="btn btn-default">Logout</button>
                </div>
            </form>
        </div>
    </div>
</nav>