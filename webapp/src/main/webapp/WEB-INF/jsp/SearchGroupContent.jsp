<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div page="${groupPage}" class="row groups-content">
    <c:forEach var="current" items="${groups}">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="media">
                        <div class="media-left">
                            <a href="${pageContext.request.contextPath}/group/${current.ID}">
                                <img class="min-photo img-thumbnail"
                                     src="${pageContext.request.contextPath}/content/group/${current.ID}"
                                     onerror="this.src='${pageContext.request.contextPath}/resources/images/NotFound.png'">
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="input-form__line">
                                <div class="input-form__label-70 div__top">Full name:</div>
                                <a class="link" href="${pageContext.request.contextPath}/group/${current.ID}">${current.name}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </c:forEach>
</div>