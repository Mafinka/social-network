package com.getjavajob.training.socialnetwork.zvyagu.ui.web.wrap;

public class PhoneWrapper {
    private String phone;

    public PhoneWrapper(String phone) {
        this.phone = validatePhone(phone);
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = validatePhone(phone);
    }

    private String validatePhone(String phone) {
        if (phone != null) {
            return phone.replaceAll("[^+\\d]", "");
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return "PhoneWrapper{" +
                "phone='" + phone + '\'' +
                '}';
    }
}
