package com.getjavajob.training.socialnetwork.zvyagu.service.impl;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.Friend;
import com.getjavajob.training.socialnetwork.zvyagu.common.Image;
import com.getjavajob.training.socialnetwork.zvyagu.dao.AccountDao;
import com.getjavajob.training.socialnetwork.zvyagu.dao.ImageDao;
import com.getjavajob.training.socialnetwork.zvyagu.service.AccountService;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.hibernate.converter.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;

@Service("accountService")
public class AccountServiceImpl implements AccountService {
    private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);
    private XStream xStream;
    @Autowired
    private AccountDao accountDao;
    @Autowired
    private ImageDao imageDao;

    @Autowired
    public AccountServiceImpl(@Autowired AccountDao accountDao, @Autowired ImageDao imageDao) {
        this.accountDao = accountDao;
        this.imageDao = imageDao;
    }

    @Autowired
    public void setxStream(XStream xStream) {
        xStream.processAnnotations(Account.class);
        xStream.registerConverter(new HibernateProxyConverter());
        xStream.registerConverter(new HibernatePersistentCollectionConverter(xStream.getMapper()));
        xStream.registerConverter(new HibernatePersistentMapConverter(xStream.getMapper()));
        xStream.registerConverter(new HibernatePersistentSortedMapConverter(xStream.getMapper()));
        xStream.registerConverter(new HibernatePersistentSortedSetConverter(xStream.getMapper()));
        this.xStream = xStream;
    }

    @Override
    @Transactional
    public void createAccount(Account account) throws Exception {
        logger.debug("creating account {}", account);
        accountDao.create(account);
        logger.info("created account with id {}", account.getID());
    }

    @Override
    @Transactional
    public Account getAccount(long id) throws Exception {
        logger.debug("getting account by id {}", id);
        Account account = accountDao.get(id);
        if (account != null) {
            // lazy load
            account.getPhones().size();
            account.getWorkPhones().size();
        }
        return account;
    }

    @Override
    @Transactional
    public Account getAccount(String email, String password) throws Exception {
        logger.debug("getting account by email '{}' and password", email);
        Account account = accountDao.getByEmail(email);
        if (account != null && account.getPassword().equals(password)) {
            // lazy load
            account.getPhones().size();
            account.getWorkPhones().size();
            return account;
        }
        return null;
    }

    @Override
    @Transactional
    public Set<Account> getAllAccounts() throws Exception {
        logger.debug("getting all accounts");
        return accountDao.getAll();
    }

    @Override
    @Transactional
    public void updateAccount(Account account) throws Exception {
        logger.info("updating account by id {}", account.getID());
        logger.debug("update account {}", account);
        accountDao.update(account);
    }

    @Override
    @Transactional
    public void deleteAccount(long accountID) throws Exception {
        logger.info("deleting account by id {}", accountID);
        accountDao.delete(accountID);
    }

    @Override
    @Transactional
    public void addFriend(Account account, Account friend) throws Exception {
        logger.debug("adding friend {} to account {} ", friend.getID(), account.getID());
        if (account.getID() != friend.getID()) {
            accountDao.addFriend(account, friend);
        } else {
            throw new IllegalArgumentException("account and friend should not be an one person");
        }
    }

    @Override
    @Transactional
    public void removeFriend(Account account, Account friend) throws Exception {
        logger.debug("removing friend {} from account {} ", friend.getID(), account.getID());
        if (account.getID() != friend.getID()) {
            accountDao.removeFriend(account, friend);
        } else {
            throw new IllegalArgumentException("account and friend should not be an one person");
        }
    }

    @Override
    @Transactional
    public Set<Friend> getFriends(Account account) throws Exception {
        logger.debug("getting friends for account {}", account.getID());
        return accountDao.getFriends(account);
    }

    @Override
    @Transactional
    public Set<Friend> getFriends(Account account, int limit, int offset) throws Exception {
        logger.debug("getting friends for account {} with limit {} and offset", account.getID(), limit, offset);
        return accountDao.getFriends(account, limit, offset);
    }

    @Override
    @Transactional
    public void createAccountImage(Account account, Image image) throws Exception {
        logger.debug("creating image for account {}", account.getID());
        imageDao.createAccountImage(account, image);
    }

    @Override
    @Transactional
    public Image getAccountImage(Account account) throws Exception {
        logger.debug("getting image for account {}", account.getID());
        return imageDao.getAccountImage(account);
    }

    @Override
    @Transactional
    public void deleteAccountImage(Account account) throws Exception {
        logger.debug("deleting image for account {}", account.getID());
        imageDao.deleteAccountImage(account);
    }

    @Override
    @Transactional
    public Set<Account> search(String pattern) throws Exception {
        logger.debug("search accounts by pattern '{}'", pattern);
        String[] words = pattern == null ? null : pattern.split("\\s");
        return accountDao.search(words);
    }

    @Override
    @Transactional
    public Set<Account> search(String pattern, int limit, int offset) throws Exception {
        logger.debug("search accounts by pattern '{}' with limit {} and offset {}", pattern, limit, offset);
        String[] words = pattern == null ? null : pattern.split("\\s");
        return accountDao.search(words, limit, offset);
    }

    @Override
    @Transactional
    public long searchCount(String pattern) throws Exception {
        logger.debug("counting accounts searching by pattern '{}'", pattern);
        String[] words = pattern == null ? null : pattern.split("\\s");
        return accountDao.searchCount(words);
    }

    @Override
    public String toXml(Account account) throws Exception {
        return xStream.toXML(newAccount(account));
    }

    @Override
    public Account fromXml(String xml) throws Exception {
        return (Account) xStream.fromXML(xml);
    }

    private Account newAccount(Account account) {
        Account result = newAccount();
        result.setID(account.getID());
        result.setFirstName(account.getFirstName());
        result.setLastName(account.getLastName());
        result.setMiddleName(account.getMiddleName());
        result.setAddress(account.getAddress());
        result.setWorkAddress(account.getWorkAddress());
        result.setBirthday(account.getBirthday());
        result.setIcq(account.getIcq());
        result.setSkype(account.getSkype());
        result.setEmail(account.getEmail());
        result.setPassword(account.getPassword());
        result.setRole(account.getRole());
        result.setExtraInfo(account.getExtraInfo());
        result.setPhones(account.getPhones());
        result.setWorkPhones(account.getWorkPhones());
        result.setRegistrationDate(account.getRegistrationDate());
        return result;
    }

    @Override
    public void close() throws Exception {
    }
}
