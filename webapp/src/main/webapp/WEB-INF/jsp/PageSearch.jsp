<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="Common.jsp" %>
<html>
<%@include file="Head.jsp" %>
<body>
<%@include file="MenuTop.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <%@include file="MenuNavigate.jsp" %>
        </div>
        <div class="col-md-7">
            <%@include file="Search.jsp" %>
        </div>
    </div>
</div>
</body>
<%@include file="Footer.jsp"%>
</html>
