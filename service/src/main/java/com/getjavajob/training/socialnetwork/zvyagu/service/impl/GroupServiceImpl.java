package com.getjavajob.training.socialnetwork.zvyagu.service.impl;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.Group;
import com.getjavajob.training.socialnetwork.zvyagu.common.Image;
import com.getjavajob.training.socialnetwork.zvyagu.dao.GroupDao;
import com.getjavajob.training.socialnetwork.zvyagu.dao.ImageDao;
import com.getjavajob.training.socialnetwork.zvyagu.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;

@Service
public class GroupServiceImpl implements GroupService {
    private static final Logger logger = LoggerFactory.getLogger(GroupServiceImpl.class);
    private GroupDao groupDao;
    private ImageDao imageDao;

    @Autowired
    public GroupServiceImpl(@Autowired GroupDao groupDao, @Autowired ImageDao imageDao) {
        this.groupDao = groupDao;
        this.imageDao = imageDao;
    }

    @Override
    @Transactional
    public Group getGroup(long id) throws Exception {
        logger.debug("getting group by id {}", id);
        Group group = groupDao.get(id);
        if (group != null) {
            group.getOwner().getID();
        }
        return group;
    }

    @Override
    @Transactional
    public void createGroup(Group group) throws Exception {
        logger.debug("creating group {}", group);
        groupDao.create(group);
        logger.info("created group with id {}", group.getID());
    }

    @Override
    @Transactional
    public void updateGroup(Group group) throws Exception {
        logger.info("updating group by id {}", group.getID());
        logger.debug("update group {}", group);
        groupDao.update(group);
    }

    @Override
    @Transactional
    public void deleteGroup(long groupID) throws Exception {
        logger.info("deleting group by id {}", groupID);
        groupDao.delete(groupID);
    }

    @Override
    @Transactional
    public Set<Group> getAllGroups() throws Exception {
        logger.debug("getting all groups");
        return groupDao.getAll();
    }

    @Override
    @Transactional
    public void createGroupImage(Group group, Image image) throws Exception {
        logger.debug("creating image for account {}", group.getID());
        imageDao.createGroupImage(group, image);
    }

    @Override
    @Transactional
    public Image getGroupImage(Group group) throws Exception {
        logger.debug("getting image for account {}", group.getID());
        return imageDao.getGroupImage(group);
    }

    @Override
    @Transactional
    public void deleteGroupImage(Group group) throws Exception {
        logger.debug("deleting image for account {}", group.getID());
        imageDao.deleteGroupImage(group);
    }

    @Override
    @Transactional
    public Set<Group> search(String pattern) throws Exception {
        logger.debug("search groups by pattern '{}'", pattern);
        String[] words = pattern == null ? null : pattern.split("\\s");
        return groupDao.search(words);
    }

    @Override
    @Transactional
    public Set<Group> search(String pattern, int limit, int offset) throws Exception {
        logger.debug("search groups by pattern '{}' with limit {} and offset {}", pattern, limit, offset);
        String[] words = pattern == null ? null : pattern.split("\\s");
        return groupDao.search(words, limit, offset);
    }

    @Override
    @Transactional
    public long searchCount(String pattern) throws Exception {
        logger.debug("counting groups searching by pattern '{}'", pattern);
        String[] words = pattern == null ? null : pattern.split("\\s");
        return groupDao.searchCount(words);
    }

    @Override
    @Transactional
    public Set<Account> getMembers(Group group, int limit, int offset) throws Exception {
        logger.debug("getting groups members for group {} with limit {} and offset {}", group.getID(), limit, offset);
        return groupDao.getMembers(group, limit, offset);
    }

    @Override
    @Transactional
    public Set<Group> getAccountGroups(Account account) throws Exception {
        logger.debug("getting groups for account {}", account.getID());
        return groupDao.getAccountGroups(account);
    }

    @Override
    public Set<Group> getAccountGroups(Account account, int limit, int offset) throws Exception {
        logger.debug("getting groups for account {} with limit {} and offset {}", account.getID(), limit, offset);
        return groupDao.getAccountGroups(account, limit, offset);
    }

    @Override
    public void close() throws Exception {
    }
}
