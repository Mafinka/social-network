package com.getjavajob.training.socialnetwork.zvyagu.dao;

import com.getjavajob.training.socialnetwork.zvyagu.common.BaseEntity;
import com.getjavajob.training.socialnetwork.zvyagu.dao.exeptions.DaoException;

import java.util.Set;

public interface SearchDao<E extends BaseEntity> {
    default Set<E> search(String[] words) throws DaoException {
        return search(words, Search.LIMIT, 0);
    }

    Set<E> search(String[] words, int limit, int offset) throws DaoException;

    long searchCount(String[] words) throws DaoException;
}
