<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-th-list"></span>
        Navigation
    </div>
    <div class="panel-body">
        <div class="list-group form">
            <a class="list-group-item" href="${pageContext.request.contextPath}/account/home">My account</a>
            <a class="list-group-item" >My groups</a>
            <a class="list-group-item" >My friends</a>
            <a class="list-group-item" >Messages</a>
        </div>
    </div>
</div>