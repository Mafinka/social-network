package com.getjavajob.training.socialnetwork.zvyagu.dao;

public interface DaoFactory extends AutoCloseable {

    AccountDao getAccountDao();

    GroupDao getGroupDao();

    ImageDao getImageDao();
}
