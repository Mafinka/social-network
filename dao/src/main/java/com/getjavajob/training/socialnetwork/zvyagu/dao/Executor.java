package com.getjavajob.training.socialnetwork.zvyagu.dao;

public interface Executor {
    void execute(DaoFactory factory) throws Exception;
}
