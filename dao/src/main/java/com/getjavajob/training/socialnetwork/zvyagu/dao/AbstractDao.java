package com.getjavajob.training.socialnetwork.zvyagu.dao;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.BaseEntity;

public abstract class AbstractDao<E extends BaseEntity> implements Dao<E> {
    private DaoService daoService;

    public AbstractDao(DaoService daoService) {
        this.daoService = daoService;
    }

    protected final Account createAccountProxy(long id) {
        return new AccountProxy(id, daoService);
    }
}
