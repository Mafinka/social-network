<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--to use with ajax--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="panel panel-default account_edit_div">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-edit"></span>
        ${edit_account_label}
    </div>
    <div class="panel-body">
        <form class="input-form" action="${pageContext.request.contextPath}${uri}/update"
              method="post" modelAttribute="account">
            <input hidden name="ID" value="${account.ID}">
            <div class="row row__item">
                <label class="col-md-4 row__form-label">Email*:</label>
                <div class="col-md-8">
                    <input class="form-control" name="email" required placeholder="Email" value="${account.email}">
                </div>
            </div>
            <div class="row row__item">
                <label class="col-md-4 row__form-label">Password*:</label>
                <div class="col-md-8">
                    <input class="form-control" name="password" type="password" required placeholder="Password" value="${account.password}">
                </div>
            </div>
            <div class="row row__item">
                <label class="col-md-4 row__form-label">Confirm password*:</label>
                <div class="col-md-8">
                    <input class="form-control" name="confirmPassword" type="password" required placeholder="Confirm password" value="${account.password}">
                </div>
            </div>
            <div class="row row__item">
                <label class="col-md-4 row__form-label">First name*:</label>
                <div class="col-md-8">
                    <input class="form-control" name="firstName" required placeholder="First name" value="${account.firstName}">
                </div>
            </div>
            <div class="row row__item">
                <label class="col-md-4 row__form-label">Last name*:</label>
                <div class="col-md-8">
                    <input class="form-control" name="lastName" required placeholder="Last name" value="${account.lastName}">
                </div>
            </div>
            <div class="row row__item">
                <label class="col-md-4 row__form-label">Middle name:</label>
                <div class="col-md-8">
                    <input class="form-control" name="middleName" placeholder="Middle name" value="${account.middleName}">
                </div>
            </div>
            <div class="row row__item">
                <label class="col-md-4 row__form-label">Address:</label>
                <div class="col-md-8">
                    <input class="form-control" name="address" placeholder="Address" value="${account.address}">
                </div>
            </div>
            <div class="row row__item">
                <label class="col-md-4 row__form-label">Work address:</label>
                <div class="col-md-8">
                    <input class="form-control" name="workAddress" placeholder="Work address" value="${account.workAddress}">
                </div>
            </div>
            <div class="row row__item">
                <label class="col-md-4 row__form-label">Phones:</label>
                <div class="col-md-8">
                    <div class="phonesBlock row__item">
                        <c:forEach var="phone" items="${account.phones}">
                            <c:set var="name" value="phones"/>
                            <c:set var="clazz" value="phone"/>
                            <%@include file="TemplatePhone.jsp" %>
                            <c:remove var="clazz" />
                        </c:forEach>
                    </div>
                    <button name="phones" type="button" class="btn btn-success add-phone-btn row__item">Add</button>
                </div>
            </div>
            <div class="row row__item">
                <label class="col-md-4 row__form-label">Work phones:</label>
                <div class="col-md-8">
                    <div class="phonesBlock row__item">
                        <c:forEach var="phone" items="${account.workPhones}">
                            <c:set var="name" value="workPhones"/>
                            <c:set var="clazz" value="workPhone"/>
                            <%@include file="TemplatePhone.jsp" %>
                            <c:remove var="clazz" />
                        </c:forEach>
                    </div>
                    <button name="workPhones" type="button" class="btn btn-success add-phone-btn row__item">Add</button>
                </div>
            </div>
            <div class="row row__item">
                <label class="col-md-4 row__form-label">Birthday:</label>
                <fmt:formatDate var="birthdayDate" value="${account.birthdayDate}" type="date" pattern="yyyy.MM.dd" />
                <div class="col-md-8">
                    <input class="col-md-8 form-control datePicker" name="birthday" placeholder="Birthday" value="${birthdayDate}">
                </div>
            </div>
            <div class="row row__item">
                <label class="col-md-4 row__form-label">ICQ:</label>
                <div class="col-md-8">
                    <input class="form-control" name="icq" placeholder="ICQ" value="${account.icq}">
                </div>
            </div>
            <div class="row row__item">
                <label class="col-md-4 row__form-label">Skype:</label>
                <div class="col-md-8">
                    <input class="form-control" name="skype" placeholder="Skype" value="${account.skype}">
                </div>
            </div>
            <div class="row row__item">
                <label class="col-md-4 row__form-label">About me:</label>
                <div class="col-md-8">
                    <textarea class="form-control" rows="4" name="extraInfo" wrap="soft" placeholder="About me" maxlength="255">${account.extraInfo}</textarea>
                </div>
            </div>
            <div class="div__spacing"></div>
            <div class="row row__item div__align-center">
                <button class="btn btn-primary form-submit-button" type="button">Confirm</button>
                <c:if test="${current == null}">
                    <button class="btn btn-default" type="button" onclick='window.open("${pageContext.request.contextPath}/login","_self")'>Cancel</button>
                </c:if>
                <c:if test="${uri.contains('edit')}">
                    <button class="btn btn-default" type="button" onclick='window.open("${pageContext.request.contextPath}/account/${account.ID}","_self")'>Cancel</button>
                </c:if>
            </div>
        </form>
    </div>
    <div id="phoneTemplate" style="display: none">
        <c:set var="name" value="%NAME%"/>
        <%@include file="TemplatePhone.jsp" %>
    </div>
    <div id="dialog-confirm" title="Подтвердите изменения">
        <p>Вы уверены, что хотите принять изменения?</p>
    </div>
</div>