package com.getjavajob.training.socialnetwork.zvyagu.service;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.Friend;
import com.getjavajob.training.socialnetwork.zvyagu.common.Image;

import java.util.Set;

public interface AccountService extends AutoCloseable {

    void createAccount(Account account) throws Exception;

    Account getAccount(long id) throws Exception;

    Account getAccount(String email, String password) throws Exception;

    Set<Account> getAllAccounts() throws Exception;

    void updateAccount(Account account) throws Exception;

    void deleteAccount(long accountID) throws Exception;

    void addFriend(Account account, Account friend) throws Exception;

    void removeFriend(Account account, Account friend) throws Exception;

    Set<Friend> getFriends(Account account) throws Exception;

    Set<Friend> getFriends(Account account, int limit, int offset) throws Exception;

    void createAccountImage(Account account, Image image) throws Exception;

    Image getAccountImage(Account account) throws Exception;

    void deleteAccountImage(Account account) throws Exception;

    Set<Account> search(String pattern) throws Exception;

    Set<Account> search(String pattern, int limit, int offset) throws Exception;

    long searchCount(String pattern) throws Exception;

    String toXml(Account account) throws Exception;

    Account fromXml(String xml) throws Exception;

    default Account newAccount() {
        return new Account();
    }
}
