package com.getjavajob.training.socialnetwork.zvyagu.dao.sql;

import com.getjavajob.training.socialnetwork.zvyagu.common.BaseEntity;
import com.getjavajob.training.socialnetwork.zvyagu.dao.Dao;
import com.getjavajob.training.socialnetwork.zvyagu.dao.exeptions.DaoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public abstract class SQLAbstractDao<O extends BaseEntity> implements Dao<O> {
    private static final Logger logger = LoggerFactory.getLogger(SQLAbstractDao.class);

    private SQLDaoFactoryImpl factory;
    @Autowired
    private ApplicationContext context;
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    public SQLAbstractDao() {
    }

    protected final SQLDaoFactoryImpl getCurrentFactory() {
        if (factory == null) {
            factory = context.getBean(SQLDaoFactoryImpl.class);
        }
        return factory;
    }

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public void create(O entity) throws DaoException {
        logger.debug("creating entity: {}", entity);
        EntityManager manager = getEntityManager();
        try {
            manager.persist(entity);
            // insert phones
            manager.flush();
            logger.info("entity created, generated id: {}", entity.getID());
        } catch (Exception e) {
            logger.error("error creating object", e);
            throw new DaoException(e);
        }
    }

    @Override
    public O get(long id) throws DaoException {
        logger.debug("getting entity {} by id {}", getEntityClass().getSimpleName(), id);
        EntityManager manager = getEntityManager();
        try {
            return manager.find(getEntityClass(), id);
        } catch (Exception e) {
            logger.error("error getting entity {} by id {}", getEntityClass().getSimpleName(), id);
            throw new DaoException(e);
        }
    }

    @Override
    public Set<O> getAll() throws DaoException {
        logger.debug("getting all entities {}", getEntityClass().getSimpleName());
        EntityManager manager = getEntityManager();
        try {
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<O> query = builder.createQuery(getEntityClass());
            Root<O> root = query.from(getEntityClass());
            query.select(root);
            return manager.createQuery(query).getResultList().stream().collect(Collectors.toSet());
        } catch (Exception e) {
            logger.error("error getting all entity", e);
            throw new DaoException(e);
        }
    }

    @Override
    public void update(O entity) throws DaoException {
        logger.debug("updating entity {}", entity);
        EntityManager manager = getEntityManager();
        try {
            manager.merge(entity);
        } catch (Exception e) {
            logger.error("error updating entity {} {}", entity.getID(), entity, e);
            throw new DaoException(e);
        }
    }

    @Override
    public void delete(long id) throws DaoException {
        EntityManager manager = getEntityManager();
        try {
            O entity = manager.find(getEntityClass(), id);
            if (entity != null) {
                logger.info("deleting entity {} by id {}", entity.getClass().getSimpleName(), id);
                manager.remove(entity);
            }
        } catch (Exception e) {
            logger.error("deleting error", e);
            throw new DaoException(e);
        }
    }

    protected O validateEntity(O entity) throws DaoException {
        if (getEntityManager().contains(entity)) {
            return entity;
        } else {
            logger.debug("loading entity {} to current entity session", entity.getID());
            return get(entity.getID());
        }
    }

    protected abstract Class<O> getEntityClass();
}
