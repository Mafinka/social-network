package com.getjavajob.training.socialnetwork.zvyagu.common;

import org.apache.commons.io.IOUtils;

import javax.persistence.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Entity
@Table(name = "IMAGES")
public class Image extends BaseEntity {
    @Column(name = "NAME")
    private String name;
    @Column(name = "EXT")
    private String ext;
    @Column(name = "CONTENT")
    private byte[] content;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileExtension() {
        return ext;
    }

    public void setFileExtension(String ext) {
        this.ext = ext;
    }

    public InputStream getContent() {
        return new ByteArrayInputStream(content);
    }

    public void setContent(InputStream content) {
        try {
            this.content = IOUtils.toByteArray(content);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "Image{" +
                "id=" + getID() +
                ", name='" + name + '\'' +
                ", ext='" + ext + '\'' +
                '}';
    }
}
