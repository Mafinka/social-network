package com.getjavajob.training.socialnetwork.zvyagu.dao;

import org.h2.tools.RunScript;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.io.BufferedInputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;

import static com.getjavajob.training.socialnetwork.zvyagu.dao.sql.SQLDaoTestUtil.DATABASE_INIT;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:dao-context-override.xml"})
public class SQLDaoServiceTest {
    @Autowired
    private DaoService daoService;
    @Autowired
    private DataSource dataSource;

    @Before
    public void init() throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            RunScript.execute(connection, new InputStreamReader(
                    new BufferedInputStream(ClassLoader.getSystemResourceAsStream(DATABASE_INIT))));
        }
    }

    @After
    public void free() throws Exception {
        daoService.close();
    }

    @Test
    @Ignore
    public void testFactoriesAreDifferent() throws Exception {
//        DaoFactory factory1 = daoService.getFactory();
//        DaoFactory factory2 = daoService.getFactory();
//        assertNotEquals(factory1, factory2);
    }

    @Test
    @Ignore
    public void testFactoryReturnsToPool() throws Exception {
//        DaoFactory factory = daoService.getFactory();
//        Set<DaoFactory> factories = new HashSet<>();
//        factories.add(factory);
//        for (int i = 0; i < 4; i++) {
//            factories.add(daoService.getFactory());
//        }
//        assertEquals(factories.size(), 5);
//        assertTrue(factories.contains(factory));
    }
}
