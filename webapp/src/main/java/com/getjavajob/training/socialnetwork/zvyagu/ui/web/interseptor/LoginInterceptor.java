package com.getjavajob.training.socialnetwork.zvyagu.ui.web.interseptor;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.constant.AccountRole;
import com.getjavajob.training.socialnetwork.zvyagu.service.Service;
import com.getjavajob.training.socialnetwork.zvyagu.ui.web.Attribute;
import com.getjavajob.training.socialnetwork.zvyagu.ui.web.ServletUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static com.getjavajob.training.socialnetwork.zvyagu.ui.web.interseptor.InterceptorUtil.LOGIN_FILTER_REG;

public class LoginInterceptor extends HandlerInterceptorAdapter {
    private static final Logger logger = LoggerFactory.getLogger(LoginInterceptor.class);
    @Autowired
    private Service service;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        logger.info("requested uri '{}'", request.getRequestURI());
        if (!isLogin(request)) {
            if (!request.getRequestURI().matches(LOGIN_FILTER_REG)) {
                logger.info("requested uri is not allowed for unknown user, trying autologin");
                if (ServletUtil.hasLoginCookies(request)) {
                    if (!ServletUtil.loginWithCookies(request)) {
                        logger.info("autologin failed, redirect to login page");
                        ServletUtil.redirectToLoginPage(request, response);
                        return false;
                    }
                    logger.info("autologin successful, current account {}", request.getSession().getAttribute(Attribute.ACCOUNT_ID));
                } else {
                    logger.info("autologin failed, redirect to login page");
                    ServletUtil.redirectToLoginPage(request, response);
                    return false;
                }
            }
        }
        prepareRequest(request);
        return true;
    }

    private void prepareRequest(HttpServletRequest request) throws Exception {
        Account current = getCurrentAccount(request.getSession());
        request.setAttribute(Attribute.CURRENT_ACCOUNT, current);
        if (current != null) {
            request.setAttribute("isAdmin", current.getRole() == AccountRole.ADMIN);
        }
        request.setAttribute("url", request.getContextPath());
        request.setAttribute("uri", request.getRequestURI());
    }

    private Account getCurrentAccount(HttpSession session) throws Exception {
        Long id = (Long) session.getAttribute(Attribute.ACCOUNT_ID);
        if (id != null) {
            return service.getAccountService().getAccount(id);
        } else {
            return null;
        }
    }

    private boolean isLogin(HttpServletRequest request) {
        return request.getSession().getAttribute(Attribute.ACCOUNT_ID) != null;
    }
}
