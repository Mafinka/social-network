package com.getjavajob.training.socialnetwork.zvyagu.common.constant;

public enum RequestType {
    FRIEND,
    GROUP_MEMBER
}
