package com.getjavajob.training.socialnetwork.zvyagu.service.impl;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.Image;
import com.getjavajob.training.socialnetwork.zvyagu.dao.AccountDao;
import com.getjavajob.training.socialnetwork.zvyagu.dao.ImageDao;
import com.getjavajob.training.socialnetwork.zvyagu.service.AccountService;
import com.thoughtworks.xstream.XStream;
import org.junit.Before;
import org.junit.Test;

import static com.getjavajob.training.socialnetwork.zvyagu.service.impl.ServiceTestUtil.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class AccountServiceTest {
    private static final String CHECK_ACCOUNT_XML = "<account>\n" +
            "  <firstName>Ivan</firstName>\n" +
            "  <lastName>Ivanov</lastName>\n" +
            "  <middleName>Ivanovich</middleName>\n" +
            "  <birthday>\n" +
            "    <time>665960400000</time>\n" +
            "    <timezone>Europe/Moscow</timezone>\n" +
            "  </birthday>\n" +
            "  <phones>\n" +
            "    <string>+79059998812</string>\n" +
            "    <string>+79059998811</string>\n" +
            "  </phones>\n" +
            "  <workPhones>\n" +
            "    <string>+79059998814</string>\n" +
            "    <string>+79059998813</string>\n" +
            "  </workPhones>\n" +
            "  <email>ivan91@ya.ru</email>\n" +
            "  <address>Privolnaya, 57</address>\n" +
            "  <workAddress>Privolnaya, 58</workAddress>\n" +
            "  <icq>12345678</icq>\n" +
            "  <skype>ivan91</skype>\n" +
            "  <extraInfo>extra</extraInfo>\n" +
            "</account>";
    //    private DaoService daoService;
    private AccountDao accountDao;
    private ImageDao imageDao;
    private AccountService service;

    @Before
    public void init() throws Exception {
        accountDao = mock(AccountDao.class);
        imageDao = mock(ImageDao.class);
        AccountServiceImpl impl = new AccountServiceImpl(accountDao, imageDao);
        impl.setxStream(new XStream());
        service = impl;
    }

    @Test
    public void createAccount() throws Exception {
        Account account = getFirstAccount();
        service.createAccount(account);
        verify(accountDao).create(any());
    }

    @Test
    public void getAccount() throws Exception {
        service.getAccount(1);
        verify(accountDao).get(1);
    }

    @Test
    public void getAccountByEmailAndPassword() throws Exception {
        when(accountDao.getByEmail(any())).thenReturn(getFirstAccount());
        service.getAccount("ivan91@ya.ru", "ivan1234");
        verify(accountDao).getByEmail(any());
    }

    @Test
    public void updateAccount() throws Exception {
        Account account = getFirstAccount();
        service.updateAccount(account);
        verify(accountDao).update(any());
    }

    @Test
    public void deleteAccount() throws Exception {
        service.deleteAccount(1);
        verify(accountDao).delete(1);
    }

    @Test
    public void addFriend() throws Exception {
        Account account = getFirstAccount();
        Account friend = getThirdAccount();
        service.addFriend(account, friend);
        verify(accountDao).addFriend(any(), any());
    }

    @Test
    public void removeFriend() throws Exception {
        Account account = getSecondAccount();
        Account friend = getThirdAccount();
        service.removeFriend(account, friend);
        verify(accountDao).removeFriend(any(), any());
    }

    @Test
    public void getFriends() throws Exception {
        Account account = getSecondAccount();
        service.getFriends(account);
        verify(accountDao).getFriends(any());
    }

    @Test
    public void createAccountImage() throws Exception {
        Account account = getFirstAccount();
        Image image = ServiceTestUtil.getFirstImage();
        service.createAccountImage(account, image);
        verify(imageDao).createAccountImage(any(), any());
    }

    @Test
    public void getAccountImage() throws Exception {
        Account account = getFirstAccount();
        service.getAccountImage(account);
        verify(imageDao).getAccountImage(any());
    }

    @Test
    public void deleteAccountImage() throws Exception {
        Account account = getFirstAccount();
        service.deleteAccountImage(account);
        verify(imageDao).deleteAccountImage(any());
    }

    @Test
    public void search() throws Exception {
        service.search("abc");
        verify(accountDao).search(any());
    }

    @Test
    public void searchWithParams() throws Exception {
        service.search("abc", 10, 0);
        verify(accountDao).search(new String[]{"abc"}, 10, 0);
    }

    @Test
    public void toXml() throws Exception {
        assertEquals(CHECK_ACCOUNT_XML, service.toXml(getFirstAccount()));
    }

    @Test
    public void fromXml() throws Exception {
        Account account = service.fromXml(getSystemResource("account.xml", "UTF-8"));
        Account checkAccount = getFirstAccount();
        account.setID(checkAccount.getID());
        account.setPassword(checkAccount.getPassword());
        account.setRole(checkAccount.getRole());
        assertEquals(checkAccount, account);
    }
}