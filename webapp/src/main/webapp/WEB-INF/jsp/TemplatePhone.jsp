<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="input-group row__item">
    <input class="form-control phone-input ${clazz}" name="${name}" type="tel" placeholder="Phone" value="${phone}"
           data-parsley-pattern="\+7 \(\d{3}\) \d{3}-\d{2}-\d{2}" data-parsley-group="account">
    <div class="input-group-btn">
        <button type="button" class="btn btn-warning del-phone-btn">Del</button>
    </div>
</div>