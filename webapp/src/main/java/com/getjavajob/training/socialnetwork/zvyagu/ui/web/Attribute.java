package com.getjavajob.training.socialnetwork.zvyagu.ui.web;

public abstract class Attribute {
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
//    public static final String SERVICE = "service";
    public static final String SAVE_LOGIN_COOKIES = "save";
    public static final String LOGIN_FAILED = "auth_failed";
    public static final String ACCOUNT_ID = "account_id";
    public static final String CURRENT_ACCOUNT = "current";
    public static final String ACCOUNT = "account";
    public static final String ACCOUNTS = "accounts";
    public static final String FRIENDS = "friends";
    public static final String GROUP = "group";
    public static final String GROUPS = "groups";
    public static final String MEMBERS = "members";
    public static final String IMAGE = "image";
    public static final String SEARCH_WORDS = "words";
    public static final String REDIRECT_URI = "redirect";
    public static final String PHONES = "phones";
    public static final String WORK_PHONES = "workPhones";

    public static final String ACCOUNTS_PAGES = "accountsPages";
    public static final String GROUPS_PAGES = "groupsPages";
//    public static final String ID = "id";
}
