package com.getjavajob.training.socialnetwork.zvyagu.service.impl;

import com.getjavajob.training.socialnetwork.zvyagu.dao.DaoService;
import com.getjavajob.training.socialnetwork.zvyagu.service.AccountService;
import com.getjavajob.training.socialnetwork.zvyagu.service.GroupService;
import com.getjavajob.training.socialnetwork.zvyagu.service.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.stereotype.Service
public class ServiceImpl implements Service {
    private static final Logger logger = LoggerFactory.getLogger(ServiceImpl.class);
    private DaoService daoService;
    private AccountService accountService;
    private GroupService groupService;

    @Autowired
    public ServiceImpl(DaoService daoService) {
        logger.info("Initializing service service");
        this.daoService = daoService;
    }

    @Override
    public AccountService getAccountService() {
        return accountService;
    }

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public GroupService getGroupService() {
        return groupService;
    }

    @Autowired
    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    @Override
    public void close() throws Exception {
        daoService.close();
    }
}
