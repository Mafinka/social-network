<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="Common.jsp" %>
<%--<c:set var="email" value="${requestScope['email']}"/>--%>
<%--<c:set var="password" value="${requestScope['password']}"/>--%>
<c:if test="${save != null}">
    <c:set var="checked" value="checked"/>
</c:if>
<html>
<%@include file="Head.jsp" %>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-lock"></span>
                    Authorisation
                </div>
                <dev class="panel-body">
                    <form class="input-form" action="${pageContext.request.contextPath}/login" method="post">
                        <c:if test="${auth_failed != null}">
                            <div class="input-form__user-input-error">Authorisation failed</div>
                        </c:if>
                        <input name="email" class="form-control row__item" placeholder="Email" required value="${email}">
                        <input name="password" class="form-control row__item" placeholder="Password" type="password" required value="${password}">
                        <div class="checkbox">
                            <label class="div__align-center">
                                <input id="save" name="save" type="checkbox" class="input-form__checkbox" ${checked}>
                                Remember me
                            </label>
                        </div>
                        <button class="btn btn-primary btn-block" type="submit">Login</button>
                        <a class="link div__align-right div__block" href="${pageContext.request.contextPath}/account/registration">Registration</a>
                    </form>
                </dev>
            </div>
        </div>
    </div>
</div>
</body>
<%@include file="Footer.jsp"%>
</html>
