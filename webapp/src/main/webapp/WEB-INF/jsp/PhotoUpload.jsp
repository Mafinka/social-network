<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-camera"></span>
        Photo
    </div>
    <div class="panel-body">
        <img class="photo img-thumbnail" src="${pageContext.request.contextPath}/content/${entity}/${id}"
             onerror="this.src='${pageContext.request.contextPath}/resources/images/NotFound.png'">
        <div class="div__spacing"></div>
        <form action="${pageContext.request.contextPath}/upload/${entity}/${id}" method="post" enctype="multipart/form-data" class="form">
            <input name="redirect" hidden type="text" value="${redirect}">
            <div class="file_upload photo_upload">
                <button type="button" class="btn btn-info btn-block">Choose photo</button>
                <div>File not chosen</div>
                <input name="image" type="file">
            </div>
            <button type="submit" class="btn btn-primary btn-block">Confirm</button>
        </form>
    </div>
</div>