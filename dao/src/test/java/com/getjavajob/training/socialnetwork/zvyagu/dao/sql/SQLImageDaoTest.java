package com.getjavajob.training.socialnetwork.zvyagu.dao.sql;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.Group;
import com.getjavajob.training.socialnetwork.zvyagu.common.Image;
import com.getjavajob.training.socialnetwork.zvyagu.dao.DaoService;
import com.getjavajob.training.socialnetwork.zvyagu.dao.ImageDao;
import com.getjavajob.training.socialnetwork.zvyagu.dao.exeptions.DaoException;
import org.h2.tools.RunScript;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;

import static com.getjavajob.training.socialnetwork.zvyagu.dao.sql.SQLDaoTestUtil.DATABASE_INIT;
import static org.apache.commons.io.IOUtils.toByteArray;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:dao-context-override.xml"})
public class SQLImageDaoTest {
    @Autowired
    private DaoService service;
    @Autowired
    private ImageDao dao;
    @Autowired
    private DataSource dataSource;
    @Autowired
    private SQLDaoFactoryImpl daoFactory;

    @Before
    public void init() throws IOException, SQLException {
        try (Connection connection = dataSource.getConnection()) {
            RunScript.execute(connection, new InputStreamReader(
                    new BufferedInputStream(ClassLoader.getSystemResourceAsStream(DATABASE_INIT))));
        }
        dao = daoFactory.getImageDao();
    }

    @After
    public void free() throws Exception {
        daoFactory.close();
    }

    @Test
    public void createQuery() throws Exception {
        Image entity = getFirstEntity();
        service.executeTransaction(factory -> dao.create(entity));
        long id = entity.getID();
        assertNotEquals(0, id);

        service.executeTransaction(factory -> {
            Image checkEntity = getFirstEntity();
            assertInputStreamEquals(checkEntity.getContent(), dao.get(id).getContent());
        });
    }

    @Test
    public void getQuery() throws Exception {
        service.executeTransaction(factory -> {
            Image entity = getFirstEntity();
            dao.create(entity);
            long id = entity.getID();

            InputStream inputStream = dao.get(id).getContent();
            InputStream checkInputStream = getFirstEntity().getContent();
            assertInputStreamEquals(checkInputStream, inputStream);
        });
    }

    @Test
    public void updateQuery() throws Exception {
        service.executeTransaction(factory -> {
            Image entity = getFirstEntity();
            long id = entity.getID();
            if (dao.get(id) == null) {
                dao.create(entity);
            }
            id = entity.getID();

            Image secondEntity = getSecondEntity();
            secondEntity.setID(id);
            dao.update(secondEntity);

            assertInputStreamEquals(getSecondEntity().getContent(), dao.get(id).getContent());
        });
    }

    @Test
    public void deleteQuery() throws Exception {
        service.executeTransaction(factory -> {
            Image entity = getFirstEntity();
            dao.create(entity);
            long id = entity.getID();

            dao.delete(id);
            assertNull(dao.get(id));
        });
    }

    @Test
    public void createAccountImage() throws Exception {
        service.executeTransaction(factory -> {
            Account account = SQLAccountDaoTest.getSuperUser();
            Image entity = getFirstEntity();
            dao.createAccountImage(account, entity);
            long id = entity.getID();

            assertInputStreamEquals(getFirstEntity().getContent(), dao.get(id).getContent());
        });
    }

    @Test
    public void getAccountImage() throws Exception {
        service.executeTransaction(factory -> {
            Account account = SQLAccountDaoTest.getSuperUser();
            Image entity = getFirstEntity();
            dao.createAccountImage(account, entity);
            long id = entity.getID();

            assertInputStreamEquals(getFirstEntity().getContent(), dao.getAccountImage(account).getContent());
        });
    }

    @Test
    public void deleteAccountImage() throws Exception {
        Account account = SQLAccountDaoTest.getSuperUser();
        Image entity = getFirstEntity();
        service.executeTransaction(factory -> dao.createAccountImage(account, entity));
        long id = entity.getID();
        service.executeTransaction(factory -> dao.deleteAccountImage(account));
        assertNull(dao.getAccountImage(account));
        assertNull(dao.get(id));
    }

    @Test
    public void createGroupImage() throws Exception {
        service.executeTransaction(factory -> {
            Group group = getFirstGroup();
            Image entity = getFirstEntity();
            dao.createGroupImage(group, entity);
            long id = entity.getID();

            assertInputStreamEquals(getFirstEntity().getContent(), dao.get(id).getContent());
            assertInputStreamEquals(getFirstEntity().getContent(), dao.getGroupImage(group).getContent());
        });
    }

    @Test
    public void getGroupImage() throws Exception {
        service.executeTransaction(factory -> {
            Group group = getFirstGroup();
            Image entity = getFirstEntity();
            dao.createGroupImage(group, entity);
            long id = entity.getID();

            assertInputStreamEquals(getFirstEntity().getContent(), dao.getGroupImage(group).getContent());
        });
    }

    @Test
    public void deleteGroupImage() throws Exception {
        Group group = getFirstGroup();
        Image entity = getFirstEntity();
        service.executeTransaction(factory -> dao.createGroupImage(group, entity));
        long id = entity.getID();
        service.executeTransaction(factory -> dao.deleteGroupImage(group));
        assertNull(dao.getGroupImage(group));
        assertNull(dao.get(id));
    }

    private void assertInputStreamEquals(InputStream firstStream, InputStream secondStream) throws IOException {
        assertArrayEquals(toByteArray(firstStream), toByteArray(secondStream));
    }

    private Image getFirstEntity() {
        Image entity = new Image();
//        entity.setID(1);
        entity.setContent(this.getClass().getClassLoader().getResourceAsStream("canada.jpg"));
        return entity;
    }

    private Image getSecondEntity() {
        Image entity = new Image();
//        entity.setID(2);
        entity.setContent(this.getClass().getClassLoader().getResourceAsStream("cat.jpg"));
        return entity;
    }

    private Group getFirstGroup() throws DaoException {
        Group group = new Group();
        group.setID(1);
        group.setName("firstG");
        Account account = daoFactory.getAccountDao().get(1);
        group.setOwner(account);
        group.addMember(account);
        group.addMember(daoFactory.getAccountDao().get(2));
        return group;
    }
}