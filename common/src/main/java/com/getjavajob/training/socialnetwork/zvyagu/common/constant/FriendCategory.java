package com.getjavajob.training.socialnetwork.zvyagu.common.constant;

public enum FriendCategory {
    FRIEND,
    BEST_FRIEND,
    UNIVERSITY
}
