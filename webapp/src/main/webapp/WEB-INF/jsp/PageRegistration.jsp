<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="Common.jsp" %>
<html>
<%@include file="Head.jsp" %>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-offset-2 col-md-7">
            <c:set var="edit_account_label" value="Registration"/>
            <%@include file="AccountEdit.jsp" %>
        </div>
    </div>
</div>
</body>
<%@include file="Footer.jsp"%>
</html>
