<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-list"></span>
        Search result
    </div>
    <div class="panel-body">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#tabs-1" role="tab" aria-controls="tabs-1" data-toggle="tab">Accounts</a>
            </li>
            <li role="presentation">
                <a href="#tabs-2" role="tab" aria-controls="tabs-2" data-toggle="tab">Groups</a>
            </li>
        </ul>
        <div class="tab-content">
            <div id="tabs-1" role="tabpanel" class="tab-pane active">
                <c:set var="accountsPage" value="1"/>
                <%@include file="SearchAccountContent.jsp"%>
                <div class="div__align-center accounts-buttons" data-page-count="${accountsPages}">
                    <button name="begin" class="btn btn-default btn-sm">
                        <span class="glyphicon glyphicon-backward nav_arrows"></span>
                    </button>
                    <button name="prev" class="btn btn-default btn-sm">
                        <span class="glyphicon glyphicon-triangle-left nav_arrows"></span>
                    </button>
                    <span class="accounts-page-counter">1</span>
                    <button name="next" class="btn btn-default btn-sm">
                        <span class="glyphicon glyphicon-triangle-right nav_arrows"></span>
                    </button>
                    <button name="end" class="btn btn-default btn-sm">
                        <span class="glyphicon glyphicon-forward nav_arrows"></span>
                    </button>
                </div>
            </div>
            <div id="tabs-2" role="tabpanel" class="tab-pane">
                <c:set var="groupPage" value="1"/>
                <%@include file="SearchGroupContent.jsp"%>
                <div class="div__align-center groups-buttons" data-page-count="${groupsPages}">
                    <button name="begin" class="btn btn-default btn-sm">
                        <span class="glyphicon glyphicon-backward nav_arrows"></span>
                    </button>
                    <button name="prev" class="btn btn-default btn-sm">
                        <span class="glyphicon glyphicon-triangle-left nav_arrows"></span>
                    </button>
                    <span class="groups-page-counter">1</span>
                    <button name="next" class="btn btn-default btn-sm">
                        <span class="glyphicon glyphicon-triangle-right nav_arrows"></span>
                    </button>
                    <button name="end" class="btn btn-default btn-sm">
                        <span class="glyphicon glyphicon-forward nav_arrows"></span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>