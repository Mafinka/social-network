package com.getjavajob.training.socialnetwork.zvyagu.service;


import com.getjavajob.training.socialnetwork.zvyagu.common.Image;

public interface Service extends AutoCloseable {
    AccountService getAccountService();

    GroupService getGroupService();

    default Image newImage() {
        return new Image();
    }
}
