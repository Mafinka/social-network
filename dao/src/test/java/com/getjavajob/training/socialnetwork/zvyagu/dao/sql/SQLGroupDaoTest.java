package com.getjavajob.training.socialnetwork.zvyagu.dao.sql;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.Group;
import com.getjavajob.training.socialnetwork.zvyagu.dao.AccountDao;
import com.getjavajob.training.socialnetwork.zvyagu.dao.GroupDao;
import com.getjavajob.training.socialnetwork.zvyagu.dao.exeptions.DaoException;
import org.h2.tools.RunScript;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.io.BufferedInputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.util.LinkedHashSet;
import java.util.Set;

import static com.getjavajob.training.socialnetwork.zvyagu.dao.sql.SQLAccountDaoTest.getFirstAccount;
import static com.getjavajob.training.socialnetwork.zvyagu.dao.sql.SQLDaoTestUtil.DATABASE_INIT;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:dao-context-override.xml"})
public class SQLGroupDaoTest {
    //    @Autowired
//    private DaoService service;
    @Autowired
    private GroupDao dao;
    @Autowired
    private AccountDao accountDao;
    @Autowired
    private DataSource dataSource;
//    @Autowired
//    private SQLDaoFactoryImpl daoFactory;

    @Before
    public void init() throws Exception {
        try (Connection connection = dataSource.getConnection()) {
            RunScript.execute(connection, new InputStreamReader(
                    new BufferedInputStream(ClassLoader.getSystemResourceAsStream(DATABASE_INIT))));
        }
    }

    @Test
    @Transactional
    public void getAll() throws Exception {
        Set<Group> entities = dao.getAll();
        assertEquals(entities, getTestGroups());
    }

    @Test
    @Transactional
    public void createQuery() throws Exception {
        Group group = getCreateTestGroup();
        assertEquals(0, group.getID());
        dao.create(group);
        long id = group.getID();
        assertNotEquals(0, id);

        Group checkGroup = getCreateTestGroup();
        checkGroup.setID(id);
        assertEquals(checkGroup, dao.get(id));
    }

    @Test
    @Transactional
    public void getQuery() throws Exception {
        Group group = dao.get(1);
        Group checkAccount = getFirstGroup();
        assertEquals(checkAccount, group);
    }

    @Test
    @Transactional
    public void updateQuery() throws Exception {
        Group entity = getSecondGroup();
        entity.setName("secondGroup");
        entity.addMember(accountDao.get(2));
        entity.removeMember(accountDao.get(3));
        entity.addMember(accountDao.get(2));
        entity.removeMember(accountDao.get(3));
        dao.update(entity);
        assertEquals(entity, dao.get(entity.getID()));
    }

    @Test
    @Transactional
    public void deleteQuery() throws Exception {
        dao.delete(3);
        assertNull(dao.get(3));
    }

    @Test
    @Transactional
    public void searchEmpty() throws Exception {
        Set<Group> result = dao.search(new String[0]);
        assertEquals(3, result.size());
    }

    @Test
    @Transactional
    public void searchNull() throws Exception {
        Set<Group> result = dao.search(null);
        assertEquals(3, result.size());
    }

    @Test
    @Transactional
    public void searchOneWord() throws Exception {
        Set<Group> result = dao.search(new String[]{"rstG"});
        assertEquals(1, result.size());
        assertTrue(result.contains(getFirstGroup()));
    }

    @Test
    @Transactional
    public void searchManyWords() throws Exception {
        Set<Group> result = dao.search(new String[]{"fi", "G"});
        assertEquals(1, result.size());
        assertTrue(result.contains(getFirstGroup()));
    }

    @Test
    @Transactional
    public void searchCountEmpty() throws Exception {
        long result = dao.searchCount(new String[0]);
        assertEquals(3, result);
    }

    @Test
    @Transactional
    public void searchCountNull() throws Exception {
        long result = dao.searchCount(null);
        assertEquals(3, result);
    }

    @Test
    @Transactional
    public void searchCountOneWord() throws Exception {
        long result = dao.searchCount(new String[]{"rstG"});
        assertEquals(1, result);
    }

    @Test
    @Transactional
    public void searchCountManyWords() throws Exception {
        long result = dao.searchCount(new String[]{"fi", "G"});
        assertEquals(1, result);
    }

    @Test
    @Transactional
    public void getMembers() throws Exception {
        System.out.println(dao.getMembers(getFirstGroup(), 1, 0));
        System.out.println(dao.getMembers(getFirstGroup(), 10, 0));
    }

    @Test
    @Transactional
    public void getAccountGroups() throws Exception {
        Set<Group> accountGroups = dao.getAccountGroups(getFirstAccount());
        assertEquals(2, accountGroups.size());
        assertTrue(accountGroups.contains(dao.get(1)));
        assertTrue(accountGroups.contains(dao.get(2)));
        assertFalse(accountGroups.contains(dao.get(3)));

        assertEquals(2, dao.getAccountGroups(getFirstAccount(), 10, 0).size());
        assertEquals(1, dao.getAccountGroups(getFirstAccount(), 1, 0).size());
    }

    private Group getCreateTestGroup() throws DaoException {
        Group group = new Group();
        group.setName("created group");
        Account account = accountDao.get(1);
        group.setOwner(account);
        group.addMember(account);
        group.addMember(accountDao.get(2));
        return group;
    }

    private Set<Group> getTestGroups() throws DaoException {
        Set<Group> result = new LinkedHashSet<>();

        result.add(getFirstGroup());

        result.add(getSecondGroup());

        Group group = new Group();
        group.setID(3);
        group.setName("thirdG");
        Account account = accountDao.get(2);
        group.setOwner(account);
        group.addMember(account);
        group.addMember(accountDao.get(3));
        result.add(group);
        return result;
    }

    private Group getFirstGroup() throws DaoException {
        Group group = new Group();
        group.setID(1);
        group.setName("firstG");
        Account account = accountDao.get(1);
        group.setOwner(account);
        group.addMember(account);
        group.addMember(accountDao.get(2));
        return group;
    }

    private Group getSecondGroup() throws DaoException {
        Group group = new Group();
        group.setID(2);
        group.setName("secondG");
        Account account = accountDao.get(3);
        group.setOwner(account);
        group.addMember(accountDao.get(1));
        group.addMember(account);
        return group;
    }

    private Group getThirdGroup() throws DaoException {
        Group group = new Group();
        group.setID(3);
        group.setName("thirdG");
        Account account = accountDao.get(2);
        group.setOwner(account);
        group.addMember(accountDao.get(3));
        group.addMember(account);
        return group;
    }
}
