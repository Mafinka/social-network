package com.getjavajob.training.socialnetwork.zvyagu.dao.sql;

import com.getjavajob.training.socialnetwork.zvyagu.dao.AccountDao;
import com.getjavajob.training.socialnetwork.zvyagu.dao.DaoFactory;
import com.getjavajob.training.socialnetwork.zvyagu.dao.GroupDao;
import com.getjavajob.training.socialnetwork.zvyagu.dao.ImageDao;
import com.getjavajob.training.socialnetwork.zvyagu.dao.exeptions.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("daoFactory")
public class SQLDaoFactoryImpl implements DaoFactory {
    private AccountDao accountDao;
    private GroupDao groupDao;
    private ImageDao imageDao;

    @Override
    public AccountDao getAccountDao() {
        return accountDao;
    }

    @Autowired
    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public GroupDao getGroupDao() {
        return groupDao;
    }

    @Autowired
    public void setGroupDao(GroupDao groupDao) {
        this.groupDao = groupDao;
    }

    @Override
    public ImageDao getImageDao() {
        return imageDao;
    }

    @Autowired
    public void setImageDao(ImageDao imageDao) {
        this.imageDao = imageDao;
    }

    @Override
    public void close() throws DaoException { }
}
