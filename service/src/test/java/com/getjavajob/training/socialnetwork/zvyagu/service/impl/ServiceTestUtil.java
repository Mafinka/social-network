package com.getjavajob.training.socialnetwork.zvyagu.service.impl;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.Friend;
import com.getjavajob.training.socialnetwork.zvyagu.common.Group;
import com.getjavajob.training.socialnetwork.zvyagu.common.Image;
import com.getjavajob.training.socialnetwork.zvyagu.common.constant.AccountRole;
import com.getjavajob.training.socialnetwork.zvyagu.dao.DaoService;
import com.getjavajob.training.socialnetwork.zvyagu.dao.exeptions.DaoException;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Calendar;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SuppressWarnings("Duplicates")
public abstract class ServiceTestUtil {
    public static DaoService getService() throws Exception {
        DaoService daoService = mock(DaoService.class);
        when(daoService.executeQuery(any())).thenReturn(null);
        return daoService;
    }

    public static String getSystemResource(String url, String encoding) throws IOException {
        return IOUtils.toString(ServiceTestUtil.class.getClassLoader().getResourceAsStream(url), encoding);
    }

    public static Account getFirstAccount() {
        Account account = new Account();
        account.setID(1);
//        account.setUsername("ivan91");
        account.setRegistrationDate(Calendar.getInstance());
        account.setFirstName("Ivan");
        account.setLastName("Ivanov");
        account.setMiddleName("Ivanovich");
        account.setBirthday(1991, 2, 8);
        account.setEmail("ivan91@ya.ru");
        account.setAddress("Privolnaya, 57");
        account.setWorkAddress("Privolnaya, 58");
        account.setIcq("12345678");
        account.setSkype("ivan91");
        account.setExtraInfo("extra");
        account.addPhone("+79059998811");
        account.addPhone("+79059998812");
        account.addWorkPhone("+79059998813");
        account.addWorkPhone("+79059998814");
        account.setPassword("ivan1234");
        account.setRole(AccountRole.USER);
        return account;
    }

    public static Account getSecondAccount() {
        Account account = new Account();
        account.setID(2);
//        account.setUsername("petr83");
        account.setFirstName("Petr");
        account.setLastName("Petrov");
        account.setMiddleName("Petrovich");
        account.setBirthday(1983, 12, 31);
        account.setEmail("-petr83-@ya.ru");
        account.setAddress("Moscow");
        account.setWorkAddress("Moscow");
        account.setIcq("12344321");
        account.setSkype("petr83");
        account.setExtraInfo("");
        account.addPhone("+79059998821");
        account.addPhone("+79059998822");
        account.addWorkPhone("+79059998823");
        account.addWorkPhone("+79059998824");
        account.setPassword("-petrov-");
        account.setRole(AccountRole.USER);
        return account;
    }

    public static Account getThirdAccount() {
        Account account = new Account();
        account.setID(3);
//        account.setUsername("sharikov77");
        account.setFirstName("Poligraph");
        account.setLastName("Sharikov");
        account.setMiddleName("Poligraphich");
        account.setBirthday(1977, 6, 12);
        account.setEmail("sharikov77@ya.ru");
        account.setAddress("Lenina, 32");
        account.setWorkAddress("Lenina, 44");
        account.setIcq("12346543");
        account.setSkype("sharikov77");
        account.setExtraInfo(null);
        account.addPhone("+79059998831");
        account.addPhone("+79059998832");
        account.addWorkPhone("+79059998833");
        account.addWorkPhone("+79059998834");
        account.setPassword("poligraf");
        account.setRole(AccountRole.USER);
        return account;
    }

    public static Image getFirstImage() {
        Image entity = new Image();
        entity.setID(1L);
        entity.setContent(new ByteArrayInputStream(new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}));
        return entity;
    }

    public static Group getFirstGroup() throws DaoException {
        Group group = new Group();
        group.setID(1);
        group.setName("firstG");
        Account account = getFirstAccount();
        group.setOwner(account);
        group.addMember(account);
        group.addMember(getSecondAccount());
        return group;
    }

    public static Group getSecondGroup() throws DaoException {
        Group group = new Group();
        group.setID(2);
        group.setName("secondG");
        Account account = getThirdAccount();
        group.setOwner(account);
        group.addMember(getFirstAccount());
        group.addMember(account);
        return group;
    }
}
