<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="Common.jsp" %>
<%--@elvariable id="current" type="com.getjavajob.training.socialnetwork.zvyagu.common.Account"--%>
<%--@elvariable id="account" type="com.getjavajob.training.socialnetwork.zvyagu.common.Account"--%>
<html>
<%@include file="Head.jsp" %>
<body>
<%@include file="MenuTop.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <%@include file="MenuNavigate.jsp" %>
        </div>
        <div class="col-md-7">
            <c:choose>
                <%--@elvariable id="page" type="java.lang.integer"--%>
                <c:when test="${page == 1}">
                    <c:set var="edit_account_label" value="Edit person information" />
                    <%@include file="AccountEdit.jsp" %>
                </c:when>
                <c:otherwise>
                    <%@include file="AccountInfo.jsp" %>
                </c:otherwise>
            </c:choose>
            <%@include file="AccountMessages.jsp" %>
        </div>
        <div class="col-md-3">
            <c:set var="entity" value="account"/>
            <c:set var="id" value="${account.ID}"/>
            <c:set var="redirect" value="${uri}"/>
            <%@include file="PhotoUpload.jsp" %>
            <c:if test="${page == 1}">
                <%@include file="AccountEditXML.jsp"%>
            </c:if>
            <%@include file="MenuAccountFriends.jsp" %>
            <%@include file="MenuAccountGroups.jsp" %>
        </div>
    </div>
</div>
</body>
<%@include file="Footer.jsp"%>
</html>