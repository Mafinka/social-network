package com.getjavajob.training.socialnetwork.zvyagu.dao.sql;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.Group;
import com.getjavajob.training.socialnetwork.zvyagu.common.Image;
import com.getjavajob.training.socialnetwork.zvyagu.dao.ImageDao;
import com.getjavajob.training.socialnetwork.zvyagu.dao.exeptions.DaoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("imageDao")
public class SQLImageDao extends SQLAbstractDao<Image> implements ImageDao {
    private static final Logger logger = LoggerFactory.getLogger(SQLImageDao.class);

    @Autowired
    public SQLImageDao() {
        super();
    }

    @Override
    public void createAccountImage(Account account, Image image) throws DaoException {
        Account validatedAccount = validateAccount(account);
        Image oldImage = getAccountImage(validatedAccount);
        if (oldImage != null) {
            image.setID(oldImage.getID());
            update(image);
        } else {
            validatedAccount.setImage(image);
            create(image);
        }
    }

    @Override
    public Image getAccountImage(Account account) throws DaoException {
        Image image = validateAccount(account).getImage();
        try {
            if (image != null) {
                image.getID();
            }
            return image;
        } catch (Exception e) {
            logger.error("error getting image for account {}", account.getID());
            throw new DaoException(e);
        }
    }

    @Override
    public void deleteAccountImage(Account account) throws DaoException {
        Image image = getAccountImage(validateAccount(account));
        if (image != null) {
            delete(image.getID());
        }
    }

    private Account validateAccount(Account account) throws DaoException {
        try {
            if (getEntityManager().contains(account)) {
                return account;
            } else {
                logger.debug("loading entity {} to current entity session", account.getID());
                return getCurrentFactory().getAccountDao().get(account.getID());
            }
        } catch (Exception e) {
            logger.error("error attaching account {} to entity manager", account, e);
            throw new DaoException(e);
        }
    }

    @Override
    public void createGroupImage(Group group, Image image) throws DaoException {
        Group validatedGroup = validateGroup(group);
        Image oldImage = getGroupImage(validatedGroup);
        if (oldImage != null) {
            image.setID(oldImage.getID());
            update(image);
        } else {
            create(image);
            validatedGroup.setImage(image);
        }
    }

    @Override
    public Image getGroupImage(Group group) throws DaoException {
        Image image = validateGroup(group).getImage();
        try {
            if (image != null) {
                image.getID();
            }
            return image;
        } catch (Exception e) {
            logger.error("error getting image for group {}", group.getID());
            throw new DaoException(e);
        }
    }

    private Group validateGroup(Group group) throws DaoException {
        if (getEntityManager().contains(group)) {
            return group;
        } else {
            logger.debug("loading entity {} to current entity session", group.getID());
            return getCurrentFactory().getGroupDao().get(group.getID());
        }
    }

    @Override
    protected Class<Image> getEntityClass() {
        return Image.class;
    }

    @Override
    public void deleteGroupImage(Group group) throws DaoException {
        Image image = getGroupImage(validateGroup(group));
        if (image != null) {
            delete(image.getID());
        }
    }
}
