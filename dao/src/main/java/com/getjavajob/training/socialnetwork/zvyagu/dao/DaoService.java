package com.getjavajob.training.socialnetwork.zvyagu.dao;

import org.springframework.transaction.annotation.Transactional;

public interface DaoService extends AutoCloseable {
    @Transactional(readOnly = true)
    void execute(Executor executor) throws Exception;

    @Transactional(readOnly = true)
    <O> O executeQuery(QueryExecutor<O> executor) throws Exception;

    @Transactional
    void executeTransaction(Executor executor) throws Exception;

    @Transactional
    <O> O executeTransactionQuery(QueryExecutor<O> executor) throws Exception;
}
