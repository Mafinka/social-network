package com.getjavajob.training.socialnetwork.zvyagu.service;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.Group;
import com.getjavajob.training.socialnetwork.zvyagu.common.Image;

import java.util.Set;

public interface GroupService extends AutoCloseable {
    Group getGroup(long id) throws Exception;

    void createGroup(Group group) throws Exception;

    void updateGroup(Group group) throws Exception;

    void deleteGroup(long groupID) throws Exception;

    Set<Group> getAllGroups() throws Exception;

    void createGroupImage(Group group, Image image) throws Exception;

    Image getGroupImage(Group group) throws Exception;

    void deleteGroupImage(Group group) throws Exception;

    Set<Group> search(String pattern) throws Exception;

    Set<Group> search(String pattern, int limit, int offset) throws Exception;

    long searchCount(String pattern) throws Exception;

    Set<Account> getMembers(Group group, int limit, int offset) throws Exception;

    Set<Group> getAccountGroups(Account account) throws Exception;

    Set<Group> getAccountGroups(Account account, int limit, int offset) throws Exception;

    default Group newGroup() {
        return new Group();
    }
}
