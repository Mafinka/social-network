<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-user"></span>
        Friends
    </div>
    <div class="panel-body">
        <c:forEach var="current" items="${friends}">
            <div class="media">
                <div class="media-left">
                    <a href="${pageContext.request.contextPath}/account/${current.account.ID}">
                        <img class="min-photo img-thumbnail"
                             src="${pageContext.request.contextPath}/content/account/${current.account.ID}"
                             onerror="this.src='${pageContext.request.contextPath}/resources/images/NotFound.png'">
                    </a>
                </div>
                <div class="media-body">
                    <div class="input-form__line">
                        <a class="link" href="${pageContext.request.contextPath}/account/${current.account.ID}">
                                ${current.account.lastName} ${current.account.firstName} ${current.account.middleName}
                        </a>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>
