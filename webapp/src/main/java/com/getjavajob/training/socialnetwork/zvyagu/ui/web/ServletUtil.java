package com.getjavajob.training.socialnetwork.zvyagu.ui.web;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.service.Service;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class ServletUtil {
    private static Service service;

    public static Service getService(ServletContext context) {
        if (service == null) {
            service = WebApplicationContextUtils.getWebApplicationContext(context).getBean(Service.class);
        }
        return service;
    }

    public static void redirectToLoginPage(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.sendRedirect(req.getContextPath() + Uri.LOGIN);
    }

    public static boolean login(HttpServletRequest req, String email, String password) throws Exception {
        Account account = getService(req.getServletContext()).getAccountService().getAccount(email, password);
        System.out.println("login account = " + account);
        if (account != null) {
            req.getSession().setAttribute(Attribute.ACCOUNT_ID, account.getID());
            return true;
        } else {
            return false;
        }
    }

    public static boolean hasLoginCookies(HttpServletRequest req) {
        boolean hasEmail = false;
        boolean hasPassword = false;
        if (req.getCookies() != null) {
            for (Cookie cookie : req.getCookies()) {
                if (Attribute.EMAIL.equals(cookie.getName())) {
                    hasEmail = true;
                }
                if (Attribute.PASSWORD.equals(cookie.getName())) {
                    hasPassword = true;
                }
            }
        }
        return hasEmail && hasPassword;
    }

    @SuppressWarnings("ConstantConditions")
    public static boolean loginWithCookies(HttpServletRequest req) throws Exception {
        String email = getCookie(req, Attribute.EMAIL).getValue();
        String password = getCookie(req, Attribute.PASSWORD).getValue();
        return login(req, email, password);
    }

    public static void saveLoginCookies(HttpServletResponse resp, String email, String password) {
        resp.addCookie(new Cookie(Attribute.EMAIL, email));
        resp.addCookie(new Cookie(Attribute.PASSWORD, password));
    }

    public static void deleteLoginCookies(HttpServletResponse resp) {
        Cookie emailCookie = new Cookie(Attribute.EMAIL, null);
        emailCookie.setMaxAge(0);
        resp.addCookie(emailCookie);

        Cookie passwordCookie = new Cookie(Attribute.PASSWORD, null);
        passwordCookie.setMaxAge(0);
        resp.addCookie(passwordCookie);
    }

    public static Cookie getCookie(HttpServletRequest req, String name) {
        if (req.getCookies() != null) {
            for (Cookie cookie : req.getCookies()) {
                if (cookie.getName().equals(name)) {
                    return cookie;
                }
            }
        }
        return null;
    }
}
