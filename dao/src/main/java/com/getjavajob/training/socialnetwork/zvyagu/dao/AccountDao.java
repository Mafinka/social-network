package com.getjavajob.training.socialnetwork.zvyagu.dao;

import com.getjavajob.training.socialnetwork.zvyagu.common.Account;
import com.getjavajob.training.socialnetwork.zvyagu.common.Friend;
import com.getjavajob.training.socialnetwork.zvyagu.common.Group;
import com.getjavajob.training.socialnetwork.zvyagu.common.constant.FriendCategory;
import com.getjavajob.training.socialnetwork.zvyagu.dao.exeptions.DaoException;

import java.util.Set;

public interface AccountDao extends Dao<Account>, SearchDao<Account> {
    Account getByEmail(String email) throws DaoException;

    Set<Friend> getFriends(Account account) throws DaoException;

    Set<Friend> getFriends(Account account, int limit, int offset) throws DaoException;

    default void addFriend(Account account, Account friend) throws DaoException {
        addFriend(account, friend, FriendCategory.FRIEND);
    }

    void addFriend(Account account, Account friend, FriendCategory category) throws DaoException;

    void removeFriend(Account account, Account friend) throws DaoException;
}
